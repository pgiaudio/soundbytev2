﻿using System;
using log4net;
using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PGI.SB2.Entity;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using System.Reflection;
using PGI.SB2.SDK;
using PGI.SB2.Core;

namespace PGI.SB2.UnitTest
{
    [TestClass]
    public class Entity
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Entity));

        public const string API_BASE_URL = "http://localhost:59199/";
        public const string TEST_BUCKET_US_NAME = "pgi-sb2-dev-asset-bucket-us1";
        public const string TEST_BUCKET_EU_NAME = "pgi-sb2-dev-asset-bucket-eu1";
        public const string TEST_BUCKET_AP_NAME = "pgi-sb2-dev-asset-bucket-ap1";
        public const string TEST_ASSET_DOMAIN_NAME = "pgi-sb2-dev-asset-domain";
        public const string TEST_ASSET_VERSION_DOMAIN_NAME = "pgi-sb2-dev-asset-version";
        public const string TEST_USER_DOMAIN_NAME = "pgi-sb2-dev-user-detail";
        public const string TEST_JPG_ASSET_ID_US = "D6F94A62-7B61-4771-8451-B7BC74B752D5";
        public const string TEST_JPG_ASSET_ID_EU = "82375629-E99E-41AE-8B7D-AE3CBE76FEB3";
        public const string TEST_JPG_ASSET_ID_AP = "AD832A3D-761F-4B1B-AE16-4AC1E0F1E219";
        public const string TEST_WAV_ASSET_ID = "2156D336-4F55-4D91-B593-EE72EE408ABA";

        public Entity()
        {
            XmlConfigurator.Configure();
        }

        [Ignore]
        [TestMethod]
        public void CreateBucket()
        {
            SB2Bucket bucket = new SB2Bucket(TEST_BUCKET_US_NAME, "us-west-2");

            Assert.IsTrue(bucket.CheckBucketExists(TEST_BUCKET_US_NAME));
        }

        [TestMethod]
        public void WriteToLog()
        {
            log.Error("This is a test error");
        }

        [Ignore]
        [TestMethod]
        public void DeleteBucket()
        {
            SB2Bucket bucket = new SB2Bucket(TEST_BUCKET_US_NAME, "us-west-2");

            if (bucket.CheckBucketExists(TEST_BUCKET_US_NAME))
            {
                bucket.DeleteBucket();
            }
            else
            {
                Assert.Fail("Bucket does not exist");
            }

            Assert.IsFalse(bucket.CheckBucketExists(TEST_BUCKET_US_NAME));
        }

        [Ignore]
        [TestMethod]
        public void AddNewAsset()
        {
            SB2Bucket bucket = new SB2Bucket(TEST_BUCKET_US_NAME, "us-west-2");
            SB2Domain assetDomain = new SB2Domain(TEST_ASSET_DOMAIN_NAME);
            SB2Domain assetVersionDomain = new SB2Domain(TEST_ASSET_VERSION_DOMAIN_NAME);

            Asset asset = new Asset();

            var assembly = Assembly.GetExecutingAssembly();
            asset.SetContent("Koala.jpg", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.jpg.Koala.jpg"));

            bool bRet = asset.Save(bucket, assetDomain, assetVersionDomain, "javram");

            Assert.IsTrue(bRet);
        }

        [TestMethod]
        public void AddAssetVersionJPG()
        {
            string bucketName = "";
            string region = "us-west-1";
            string assetId = "";

            Random rndRegion = new Random(System.Environment.TickCount);
            int regionValue = rndRegion.Next(1, 3);
            switch (regionValue)
            {
                case 1:
                    bucketName = TEST_BUCKET_US_NAME;
                    region = "us-west-1";
                    assetId = TEST_JPG_ASSET_ID_US;
                    break;
                case 2:
                    bucketName = TEST_BUCKET_EU_NAME;
                    region = "eu-west-1";
                    assetId = TEST_JPG_ASSET_ID_EU;
                    break;
                case 3:
                    bucketName = TEST_BUCKET_AP_NAME;
                    region = "ap-northeast-1";
                    assetId = TEST_JPG_ASSET_ID_AP;
                    break;
            }

            SB2Bucket bucket = new SB2Bucket(bucketName, region);
            SB2Domain assetDomain = new SB2Domain(TEST_ASSET_DOMAIN_NAME);
            SB2Domain assetVersionDomain = new SB2Domain(TEST_ASSET_VERSION_DOMAIN_NAME);

            Asset asset = new Asset(new System.Guid(assetId));
            asset.Retrieve(assetDomain, assetVersionDomain);
            if (!asset.Exists)
            {
                asset.CompanyID = "1234";
                asset.CompanyName = "PGI";
                asset.ClientID = "452345";
                asset.ModeratorName = "John Avram";
                asset.ModeratorEmail = "johnathan.avram@pgi.com";
                asset.ConferenceID = "888";
                asset.ConferenceLink = "0008880100415162148";
                asset.ConferenceName="Test Conference JPG";
                asset.EventStartUTC = System.DateTime.UtcNow;
                asset.EventEndUTC = System.DateTime.UtcNow.AddHours(1);
                asset.Notes = "Here are some notes about a .jpg file";
            };

            var assembly = Assembly.GetExecutingAssembly();
            Random rnd1 = new Random(System.Environment.TickCount);
            int number = rnd1.Next(1, 4);
            switch (number)
            {
                case 1:
                    asset.SetContent("Koala.jpg", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.jpg.Koala.jpg"));
                    break;
                case 2:
                    asset.SetContent("Lighthouse.jpg", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.jpg.Lighthouse.jpg"));
                    break;
                case 3:
                    asset.SetContent("Penguins.jpg", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.jpg.Penguins.jpg"));
                    break;
                case 4:
                    asset.SetContent("Tulips.jpg", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.jpg.Tulips.jpg"));
                    break;
            }

            bool bRet = asset.Save(bucket, assetDomain, assetVersionDomain, "javram");

            Assert.IsTrue(bRet);
        }

        [TestMethod]
        public void AddAssetVersionWAV()
        {
            SB2Bucket bucket = new SB2Bucket(TEST_BUCKET_EU_NAME, "us-west-2");
            SB2Domain assetDomain = new SB2Domain(TEST_ASSET_DOMAIN_NAME);
            SB2Domain assetVersionDomain = new SB2Domain(TEST_ASSET_VERSION_DOMAIN_NAME);

            Asset asset = new Asset(new System.Guid(TEST_WAV_ASSET_ID));
            asset.Retrieve(assetDomain, assetVersionDomain);
            if (!asset.Exists)
            {
                asset.CompanyID = "1234";
                asset.CompanyName = "PGI";
                asset.ClientID = "452345";
                asset.ModeratorName = "John Avram";
                asset.ModeratorEmail = "johnathan.avram@pgi.com";
                asset.ConferenceID = "888";
                asset.ConferenceLink = "0008880100415162148";
                asset.ConferenceName = "Test Conference JPG";
                asset.EventStartUTC = System.DateTime.UtcNow;
                asset.EventEndUTC = System.DateTime.UtcNow.AddHours(1);
                asset.Notes = "Here are some notes about a .wav file";
            };

            var assembly = Assembly.GetExecutingAssembly();
            Random rnd1 = new Random(System.Environment.TickCount);
            int number = rnd1.Next(1, 4);
            switch (number)
            {
                case 1:
                    asset.SetContent("bond.wav", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.wav.bond.wav"));
                    break;
                case 2:
                    asset.SetContent("booboo.wav", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.wav.booboo.wav"));
                    break;
                case 3:
                    asset.SetContent("goodevening.wav", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.wav.goodevening.wav"));
                    break;
                case 4:
                    asset.SetContent("terminator.wav", assembly.GetManifestResourceStream("PGI.AVS.UnitTest.Content.wav.terminator.wav"));
                    break;
            }

            bool bRet = asset.Save(bucket, assetDomain, assetVersionDomain, "javram");

            Assert.IsTrue(bRet);
        }

        [TestMethod]
        public void AddUser()
        {
            SB2Domain userDomain = new SB2Domain(TEST_USER_DOMAIN_NAME);

            User user = new User();
            user.UserName = "javram";
            user.Password = "1234";
            user.Email = "johnathan.avram@pgi.com";
            user.Active = true;

            Assert.IsTrue(user.Save(userDomain));
        }

        [TestMethod]
        public void AddManagementConsoleUser()
        {
            SB2Domain userDomain = new SB2Domain(TEST_USER_DOMAIN_NAME);

            User user = new User();
            user.UserName = "ManagementConsole";
            user.Password = "w9AV8gAflryfZTtbReqL";
            user.Email = "soudbyte.admin@pgi.com";
            user.Role = "Console";
            user.Active = true;

            Assert.IsTrue(user.Save(userDomain));
        }

        [TestMethod]
        public void TestLoginCorrect()
        {
            SB2Domain userDomain = new SB2Domain(TEST_USER_DOMAIN_NAME);

            User user = new User();

            Assert.IsTrue(user.Login(userDomain, "javram", "1234"));
        }

        [TestMethod]
        public void TestLoginInvalid()
        {
            SB2Domain userDomain = new SB2Domain(TEST_USER_DOMAIN_NAME);

            User user = new User();

            Assert.IsFalse(user.Login(userDomain, "javram", "4321"));
        }


    }
}
