﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PGI.SB2.Entity;

namespace PGI.SB2.Plugins
{
    /// <summary>
    /// Interface used for Event processing whenever a new asset is created/modified
    /// </summary>
    public interface IAssetEvent
    {
        /// <summary>
        /// Called whenever an asset is created
        /// </summary>
        bool AssetCreated(Asset NewAsset);

        /// <summary>
        /// Called whenever an existing asset is modified
        /// </summary>
        bool AssetNewVersion(Asset NewAsset);

        /// <summary>
        /// A unique integer that identifies this plugin
        /// </summary>
        /// <returns></returns>
        int PluginIdentifier
        {
            get;
        }

        /// <summary>
        /// A display name for the plugin
        /// </summary>
        /// <returns></returns>
        string PluginName
        {
            get;
        }

        string AssemblyName
        {
            get;
        }

        Exception LastError
        {
            get;
        }
    }
}