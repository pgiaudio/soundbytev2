﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using PGI.SB2.Entity;

namespace PGI.SB2.Plugins
{
    public class SendEmailNotification : IAssetEvent
    {
        private const int PLUGIN_IDENTIFIER = 300;
        private const string PLUGIN_NAME = "SendEmailNotification";
        private static readonly ILog log = LogManager.GetLogger(typeof(SendEmailNotification));
        private static string _namespace = typeof(SendEmailNotification).Namespace;
        private Exception _lastError;

        public SendEmailNotification()
        {
            XmlConfigurator.Configure();
        }


        /// <summary>
        /// Retrieves the From Address for the emails 
        /// </summary>
        /// <returns>string</returns>
        private string FromAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["EmailFromAddress"]))
                    return System.Configuration.ConfigurationManager.AppSettings["EmailFromAddress"];
                else
                    return "";
            }
        }

        public bool AssetCreated(Asset NewAsset)
        {
            bool bRet = SendEmail(NewAsset.ModeratorEmail, FromAddress, "New Asset Added Email", "A new asset has been added, you can download it here: " + NewAsset.DownloadURL() );
            log.Info("PGI.AVS.Services.AssetCreated: " + NewAsset.Name);
            return bRet;
        }

        public bool AssetNewVersion(Asset NewAsset)
        {
            bool bRet = SendEmail(NewAsset.ModeratorEmail, FromAddress, "New Asset Version Added Email", "A new asset version has been added, you can download it here: " + NewAsset.DownloadURL());
            log.Info("PGI.AVS.Services.AssetNewVersion: " + NewAsset.Name);
            return bRet;
        }

        public int PluginIdentifier
        {
            get
            {
                return PLUGIN_IDENTIFIER;
            }
        }

        public string PluginName
        {
            get 
            {
                return PLUGIN_NAME;
            }
        }

        public string AssemblyName
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().FullName;
            }
        }

        public Exception LastError
        {
            get
            {
                return _lastError;
            }
        }

        /// <summary>
        /// A simple SMTP service for sending emails
        /// </summary>
        /// <param name="To">to Email address</param>
        /// <param name="From">from Email address</param>
        /// <param name="Subject">Email Subject</param>
        /// <param name="MessageBody">Message Body</param>
        /// <returns>void</returns>
        private bool SendEmail(string To, string From, string Subject, string MessageBody)
        {
            try
            {
                //if (!Regex.IsMatch(address.Trim(), EMAIL_REGEX))
                //{
                //    invalidEmails.Add(address.Trim());
                //}

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.To.Add(To);
                message.From = new System.Net.Mail.MailAddress(From);
                message.Subject = Subject;
                message.Body = MessageBody;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Send(message);

                message.Dispose();
                return true;
            }
            catch(Exception ex)
            {
                _lastError = ex;
                log.Error(_namespace + ".SendEmail, to:" + To + ", from:" + From + ", subject: " + Subject + ", message: " + MessageBody, ex);
                return false;
            }
        }

    }
}
