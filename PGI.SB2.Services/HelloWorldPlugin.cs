﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using PGI.SB2.Entity;

namespace PGI.SB2.Plugins
{
    public class HelloWorldPlugin : IAssetEvent
    {
        private const int PLUGIN_IDENTIFIER = 100;
        private const string PLUGIN_NAME = "HelloWorldPlugin";
        private static readonly ILog log = LogManager.GetLogger(typeof(HelloWorldPlugin));
        private static string _namespace = typeof(HelloWorldPlugin).Namespace;
        private Exception _lastError = null;

        public HelloWorldPlugin()
        {
            XmlConfigurator.Configure();
        }

        public bool AssetCreated(Asset NewAsset)
        {
            log.Info("PGI.SB2.Plugins.HelloWorldPlugin: " + NewAsset.Name);
            return true;
        }

        public bool AssetNewVersion(Asset NewAsset)
        {
            
            try
            {
                log.Info("PGI.SB2.Plugins.HelloWorldPlugin: " + NewAsset.Name);

                throw new ApplicationException("Hello World Failed because of something");

                return true;
            }
            catch(Exception ex)
            {
                _lastError = ex;
                return false;
            }
            
        }

        public int PluginIdentifier
        {
            get
            {
                return PLUGIN_IDENTIFIER;
            }
        }

        public string PluginName
        {
            get
            {
                return PLUGIN_NAME;
            }
        }

        public string AssemblyName
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().FullName;
            }
        }

        public Exception LastError
        {
            get
            {
                return _lastError;
            }
        }
    }
}
