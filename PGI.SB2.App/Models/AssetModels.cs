﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web;
using System.Web.Security;
using PGI.SB2.SDK;
using PGI.SB2.App.Utilities;

namespace PGI.SB2.App.Models
{
    public class AssetModels
    {
    }

    public class AssetUploadModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Company ID")]
        public string CompanyID { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Client ID")]
        public string ClientID { get; set; }

        [Required]
        [Display(Name = "Moderator Name")]
        public string ModeratorName { get; set; }

        [Required]
        [Display(Name = "Moderator Email")]
        public string ModeratorEmail { get; set; }

        [Display(Name = "Conference ID")]
        public string ConferenceID { get; set; }

        [Display(Name = "Conference Name")]
        public string ConferenceName { get; set; }

        [Display(Name = "Conference Link")]
        public string ConferenceLink { get; set; }

        [Display(Name = "Event Start")]
        public string EventStart { get; set; }

        [Display(Name = "Event End")]
        public string EventEnd { get; set; }

        [Required]
        [Display(Name = "Region Name")]
        public string RegionName { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }
    }

    public class AssetAddVersionModel
    {
        [Display(Name = "Asset ID")]
        public string AssetID { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }
    }

    public class AssetDisplayViewModel
    {
        public PGI.SB2.SDK.ReadAsset Asset { get; set; }
        public List<PluginDescription> Plugins { get; set; }

        /// <summary>
        /// A helper method to resolve the plugin name for display purposes.
        /// </summary>
        /// <param name="Identifier"></param>
        /// <returns></returns>
        public string GetPluginNameForId(int Identifier)
        {
            if (Plugins != null)
            {
                PluginDescription plugin = Plugins.FirstOrDefault(p => p.Identifier == Identifier);
                if (plugin != null)
                {
                    return plugin.Name + " (" + Identifier.ToString() + ")";
                }
            }   

            return Identifier.ToString();  
        }
    }

    public class AssetSearchResultModel
    {
        public string SearchPhrase { get; set; }
        public int ResultCount { get; set; }
        public int ResultsPerPage { get; set; }
        public int PageCount { get; set; }
        public int CurrentPage { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }

        public List<AssetSearchResult> Results { get; set; }

        public AssetSearchResultModel()
        {
            Results = new List<AssetSearchResult>();
        }
    }
}