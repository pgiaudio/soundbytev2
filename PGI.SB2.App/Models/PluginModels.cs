﻿using PGI.SB2.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PGI.SB2.App.Models
{
    public class ListPluginsModel
    {
        public List<PluginDescription> Plugins { get; set; }
    }
}