﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PGI.SB2.SDK;
using PGI.SB2.App.Models;
using PGI.SB2.App.Utilities;

namespace PGI.SB2.App.Controllers
{
    [SessionAuthorize]
    public class AssetController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AssetController));

        public AssetController()
        {
            XmlConfigurator.Configure();
        }

        // GET: /Asset/
        public ActionResult Index()
        {
            AssetSearchResultModel model = new AssetSearchResultModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string SearchPhrase, int CurrentPage = 0)
        {
            AssetSearchResultModel model = new AssetSearchResultModel();
            model.SearchPhrase = SearchPhrase;
            model.CurrentPage = CurrentPage;

            try
            {
                ReadSession session = ViewBag.Session;
                //If the assetID is not specified, than navigate to an error page
                if (session != null && SearchPhrase != null)
                {
                    SearchAssetsRequest request = new SearchAssetsRequest()
                    {
                        BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                        AccessKey = session.UserName,
                        SecretKey = session.SecretKey,
                        Phrase = SearchPhrase,
                        CurrentPage = CurrentPage,
                        ResultsPerPage = 20
                    };

                    SearchAssetsResponse response = Client.SearchAssets(request);

                    if (response != null && response.StatusCode == 200)
                    {

                        model.PageCount = response.Detail.PageCount;
                        model.ResultCount = response.Detail.ResultCount;
                        model.ResultsPerPage = response.Detail.ResultsPerPage;
                        model.Results = response.Detail.Results.ToList();

                        if (model.CurrentPage < 5)
                        {
                            model.StartPage = 0;
                        }
                        else
                        {
                            model.StartPage = model.CurrentPage - 5;
                        }

                        if (model.StartPage + 10 > model.PageCount)
                        {
                            model.EndPage = model.PageCount;
                        }
                        else
                        {
                            model.EndPage = model.StartPage + 10;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }

        public ActionResult Display(string id, string versionId)
        {
            try
            {
                ReadSession session = ViewBag.Session;
                //If the assetID is not specified, than navigate to an error page
                if (session != null && id != null)
                {


                    ReadAssetRequest request = new ReadAssetRequest()
                    {
                        BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                        AccessKey = session.UserName,
                        SecretKey = session.SecretKey,
                        AssetId = new System.Guid(id),
                        VersionId = versionId
                    };

                    ReadAssetResponse response = Client.ReadAsset(request);

                    if (response != null && response.StatusCode == 200)
                    {
                        var model = new AssetDisplayViewModel();
                        model.Asset = response.Detail;

                        ListPluginsRequest piRequest = new ListPluginsRequest()
                        {
                            BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                            AccessKey = session.UserName,
                            SecretKey = session.SecretKey
                        };

                        ListPluginsResponse piResponse = Client.ListPlugins(piRequest);
                        model.Plugins = piResponse.Detail.Plugins;

                        return View(model);
                    }
                }
            }
            catch
            {

            }

            return RedirectToAction("Index", "Asset");
        }

        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(AssetUploadModel model, HttpPostedFileBase File)
        {
            try
            {
                ReadSession session = ViewBag.Session;
                if (session != null)
                {
                    //If the assetID is not specified, than navigate to an error page
                    CreateAssetRequest request = new CreateAssetRequest()
                    {
                        BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                        AccessKey = session.UserName,
                        SecretKey = session.SecretKey
                    };

                    request.Asset = new CreateAsset();
                    request.Asset.Name = model.Name;
                    request.Asset.CompanyID = model.CompanyID;
                    request.Asset.CompanyName = model.CompanyName;
                    request.Asset.ClientID = model.ClientID;
                    request.Asset.ModeratorName = model.ModeratorName;
                    request.Asset.ModeratorEmail = model.ModeratorEmail;
                    request.Asset.ConferenceID = model.ConferenceID;
                    request.Asset.ConferenceName = model.ConferenceName;
                    request.Asset.ConferenceLink = model.ConferenceLink;
                    request.Asset.EventStartUTC = model.EventStart;
                    request.Asset.EventEndUTC = model.EventEnd;
                    request.Asset.RegionName = model.RegionName;
                    request.Asset.Notes = model.Notes;

                    request.FileName = File.FileName;
                    request.InputStream = File.InputStream;

                    CreateAssetResponse response = Client.CreateAsset(request);

                    if (response.StatusCode == 200)
                    {
                        return this.RedirectToAction("Display", "Asset", new
                        {
                            id = response.AssetID.ToString()
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }

        public ActionResult AddVersion(string id)
        {
            AssetAddVersionModel model = new AssetAddVersionModel();
            model.AssetID = id;

            ReadSession session = ViewBag.Session;

            if (session != null && !string.IsNullOrEmpty(model.AssetID))
            {
                ReadAssetRequest request = new ReadAssetRequest()
                {
                    BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                    AccessKey = session.UserName,
                    SecretKey = session.SecretKey,
                    AssetId = new System.Guid(id)
                };

                ReadAssetResponse response = Client.ReadAsset(request);

                if (response != null && response.StatusCode == 200)
                {
                    model.Name = response.Detail.Name;
                    return View(model);
                }
            }

            return RedirectToAction("Index", "Asset");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddVersion(AssetAddVersionModel model, HttpPostedFileBase File)
        {
            try
            {
                ReadSession session = ViewBag.Session;
                if (session != null)
                {
                    //If the assetID is not specified, than navigate to an error page
                    UpdateAssetRequest request = new UpdateAssetRequest()
                    {
                        AssetID = new System.Guid(model.AssetID),
                        Notes = model.Notes,
                        BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                        AccessKey = session.UserName,
                        SecretKey = session.SecretKey
                    };

                    request.FileName = File.FileName;
                    request.InputStream = File.InputStream;

                    UpdateAssetResponse response = Client.AddVersion(request);

                    if (response.StatusCode == 200)
                    {
                        return this.RedirectToAction("Display", "Asset", new
                        {
                            id = response.AssetID.ToString()
                        });
                    }
                }

                throw new ApplicationException(ContentFactory.GetContent("error_asset_save"));
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            //SB2Domain assetDomain = new SB2Domain(Settings.AWSAssetDomain);
            //SB2Domain assetVersionDomain = new SB2Domain(Settings.AWSAssetVersionDomain);

            //PGI.SB2.Entity.Asset asset = new PGI.SB2.Entity.Asset(new System.Guid(model.AssetID));
            //if (asset.Retrieve(assetDomain, assetVersionDomain))
            //{
            //    //Notes can be set for a new asset or when adding a new version to an existing asset
            //    asset.Notes = model.Notes;

            //    //Amazon.RegionEndpoint storageRegion = Region.GetRegionByName(asset.RegionName);
            //    string bucketName = Settings.GetBucketNameByRegion(asset.RegionName);

            //    SB2Bucket bucket = new SB2Bucket(bucketName, asset.RegionName);
            //    //Set the new version of the file content
            //    asset.SetContent(File.FileName, File.InputStream);
            //    //Save the asset
            //    if (asset.Save(bucket, assetDomain, assetVersionDomain, User.Identity.Name))
            //    {
            //        //// Fire the notification events for all plugins
            //        //IAssetEvent events = ObjectFactory.GetInstance<IAssetEvent>();
            //        //events.AssetNewVersion(asset);

            //        return this.RedirectToAction("Display", "Asset", new
            //        {
            //            id = asset.ID.ToString()
            //        });
            //    }
            //    else
            //    {
            //        ModelState.AddModelError("", ContentFactory.GetContent("error_asset_save"));
            //        return View(model);
            //    }
            //}

            return View(model);
        }

        public ActionResult RunPlugins(string id, string versionId)
        {
            ReadSession session = ViewBag.Session;

            if (session != null && !string.IsNullOrEmpty(id))
            {
                //If the assetID is not specified, than navigate to an error page
                RunPluginsRequest request = new RunPluginsRequest()
                {
                    BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                    AccessKey = session.UserName,
                    SecretKey = session.SecretKey,
                    AssetID = new System.Guid(id),
                    VersionID = versionId
                };

                RunPluginsResponse response = Client.RunPlugins(request);
            }

            return RedirectToAction("Display", "Asset", new { id = id, versionId = versionId });
        }

    }
}
