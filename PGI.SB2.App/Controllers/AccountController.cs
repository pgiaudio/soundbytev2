﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using PGI.SB2.App.Models;
using PGI.SB2.App.Utilities;
using PGI.SB2.SDK;

namespace PGI.SB2.App.Controllers
{
    public class AccountController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AccountController));

        public AccountController()
        {
            XmlConfigurator.Configure();
        }

        //
        // GET: /Account/Login

        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            CreateSessionRequest request = new CreateSessionRequest()
            {
                BaseURL = Config.Settings.APIBaseURL,
                AccessKey = Config.Settings.ConsoleAccessKey,
                SecretKey = Config.Settings.ConsoleSecretKey,
                UserName = model.UserName,
                Password = model.Password
            };

            CreateSessionResponse response = Client.CreateSession(request);
            if (response.StatusCode == 200)
            {
                HttpCookie sessionCookie = new HttpCookie(Config.Settings.SessionCookieName, response.SessionID.ToString());

                if (model.RememberMe == true)
                {
                    sessionCookie.Expires = DateTime.Now.AddDays(14);
                }
                else
                {
                    sessionCookie.Expires = DateTime.Now.AddDays(1);
                }

                Response.Cookies.Add(sessionCookie);
                return RedirectToAction("Index", "Home");
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", ContentFactory.GetContent("error_incorrect_login"));
            return View(model);
        }

        public ActionResult LogOff()
        {
            HttpCookie sessionCookie = HttpContext.Request.Cookies[Config.Settings.SessionCookieName];
            if (sessionCookie != null && !string.IsNullOrEmpty(sessionCookie.Value))
            {
                DeleteSessionRequest request = new DeleteSessionRequest()
                {
                    BaseURL = Config.Settings.APIBaseURL,
                    AccessKey = Config.Settings.ConsoleAccessKey,
                    SecretKey = Config.Settings.ConsoleSecretKey,
                    SessionId = new System.Guid(sessionCookie.Value)
                };

                DeleteSessionResponse response = Client.DeleteSession(request);
                if (response.Result == true)
                {
                    sessionCookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Response.Cookies.Add(sessionCookie);
                }

            }

            return RedirectToAction("Index", "Home");
        }
    }
}
