﻿using PGI.SB2.App.Utilities;
using PGI.SB2.Core;
using PGI.SB2.SDK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PGI.SB2.App.Controllers
{
    [SessionAuthorize]
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create100()
        {
            string appPath = System.Web.Hosting.HostingEnvironment.MapPath(System.Web.HttpRuntime.AppDomainAppVirtualPath);

            ReadSession session = ViewBag.Session;

            for (int i = 301; i < 600; i++)
            {
                Random rnd1 = new Random(System.Environment.TickCount);

                CreateAssetRequest request = new CreateAssetRequest()
                {
                    BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                    AccessKey = session.UserName,
                    SecretKey = session.SecretKey
                };

                request.Asset = new CreateAsset();

                request.Asset.CompanyID = "Company ABC";
                request.Asset.CompanyName = "Test Company";
                request.Asset.ClientID = rnd1.Next(100000, 999999).ToString();
                request.Asset.ModeratorName = "Test Moderator";
                request.Asset.ModeratorEmail = "test@test.com";
                request.Asset.ConferenceID = rnd1.Next(100000, 999999).ToString();
                request.Asset.ConferenceName = "Conference #" + rnd1.Next(100000, 999999).ToString();
                request.Asset.ConferenceLink = rnd1.Next(100000, 999999).ToString();
                request.Asset.EventStartUTC = System.DateTime.Now.ToUniversalTime().ToString(Constants.UTC_FORMAT);
                request.Asset.EventEndUTC = System.DateTime.Now.AddHours(1).ToUniversalTime().ToString(Constants.UTC_FORMAT);
                request.Asset.RegionName = "us-west-2";
                request.Asset.Notes = "Test Notes for Asset #" + i.ToString();

                int number = rnd1.Next(1, 8);
                switch (number)
                {
                    case 1:
                        request.Asset.Name = "Bond_" + i.ToString();
                        request.FileName = "bond.wav";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\wav\\bond.wav", FileMode.Open, FileAccess.Read);
                        break;
                    case 2:
                        request.Asset.Name = "Booboo_" + i.ToString();
                        request.FileName = "booboo.wav";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\wav\\booboo.wav", FileMode.Open, FileAccess.Read);
                        break;
                    case 3:
                        request.Asset.Name = "GoodEvening_" + i.ToString();
                        request.FileName = "goodevening.wav";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\wav\\goodevening.wav", FileMode.Open, FileAccess.Read);
                        break;
                    case 4:
                        request.Asset.Name = "Terminator_" + i.ToString();
                        request.FileName = "terminator.wav";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\wav\\terminator.wav", FileMode.Open, FileAccess.Read);
                        break;
                    case 5:
                        request.Asset.Name = "Koala_" + i.ToString();
                        request.FileName = "Koala.jpg";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\jpg\\Koala.jpg", FileMode.Open, FileAccess.Read);
                        break;
                    case 6:
                        request.Asset.Name = "Lighthouse_" + i.ToString();
                        request.FileName = "Lighthouse.jpg";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\jpg\\Lighthouse.jpg", FileMode.Open, FileAccess.Read);
                        break;
                    case 7:
                        request.Asset.Name = "Penguins_" + i.ToString();
                        request.FileName = "Penguins.jpg";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\jpg\\Penguins.jpg", FileMode.Open, FileAccess.Read);
                        break;
                    case 8:
                        request.Asset.Name = "Tulips_" + i.ToString();
                        request.FileName = "Tulips.jpg";
                        request.InputStream = new FileStream(appPath + "\\Content\\Tests\\jpg\\Tulips.jpg", FileMode.Open, FileAccess.Read);
                        break;
                }


                CreateAssetResponse response = Client.CreateAsset(request);
            }

            return RedirectToAction("Index");
        }
    }
}
