﻿using PGI.SB2.App.Models;
using PGI.SB2.App.Utilities;
using PGI.SB2.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PGI.SB2.App.Controllers
{
    [SessionAuthorize]
    public class PluginController : Controller
    {
        //
        // GET: /Plugin/

        public ActionResult Index()
        {
            ListPluginsModel model = new ListPluginsModel();

            try
            {
                ReadSession session = ViewBag.Session;
                if (session != null)
                {

                    ListPluginsRequest request = new ListPluginsRequest()
                    {
                        BaseURL = System.Configuration.ConfigurationManager.AppSettings["APIBaseURL"],
                        AccessKey = session.UserName,
                        SecretKey = session.SecretKey
                    };

                    ListPluginsResponse response = Client.ListPlugins(request);
                    model.Plugins = response.Detail.Plugins;
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }

    }
}
