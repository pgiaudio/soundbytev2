﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace PGI.SB2.App.Utilities
{
    public static class Config
    {
        private static DynamicSettings _instance = new DynamicSettings();

        public static dynamic Settings
        {
            get { return _instance; }
        }
    }

    public class DynamicSettings : DynamicObject
    {
        public DynamicSettings()
        {
            items = ConfigurationManager.AppSettings;
        }
        private readonly NameValueCollection items;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = items.Get(binder.Name.ToLower());

            //We always return true in case the value isn't found; it will return null with no exceptions.
            return true;
        }
    }
}