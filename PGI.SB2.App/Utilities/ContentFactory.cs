using System;
using System.Reflection;
using System.ComponentModel;
using System.Resources;

namespace PGI.SB2.App.Utilities
{
    public static class ContentFactory
    {
        public static string GetContent(string keyName, string pageName = "Language")
        {
            // Given the pageName and keyName, get the content value from the resource file for the page.
            try
            {
                var resourceTypeString = "PGI.SB2.App.App_LocalResources." + pageName;
                var resourceType = Type.GetType(resourceTypeString);
                var propType = resourceType.GetProperty(keyName, BindingFlags.Static | BindingFlags.Public);
                return propType.GetValue(null, null) as string;
            }
            catch
            {
                return keyName;
            }
        }
    }
}