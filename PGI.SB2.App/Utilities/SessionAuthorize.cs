﻿using PGI.SB2.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PGI.SB2.App.Utilities
{
    public class SessionAuthorize : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpCookie sessionCookie = filterContext.RequestContext.HttpContext.Request.Cookies[Config.Settings.SessionCookieName];
            if (sessionCookie != null && !string.IsNullOrEmpty(sessionCookie.Value))
            {
                ReadSessionRequest request = new ReadSessionRequest()
                {
                    BaseURL = Config.Settings.APIBaseURL,
                    AccessKey = Config.Settings.ConsoleAccessKey,
                    SecretKey = Config.Settings.ConsoleSecretKey,
                    SessionId = new System.Guid(sessionCookie.Value)
                };

                ReadSessionResponse response = Client.ReadSession(request);
                if (response.StatusCode == 200)
                {
                    filterContext.Controller.ViewBag.Session = response.Detail;
                }
                else
                {
                    //Expire the cookie.
                    sessionCookie.Expires = DateTime.Now.AddDays(-1);
                    filterContext.HttpContext.Response.Cookies.Add(sessionCookie);

                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new { controller = "Account", action = "Login" }));
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { controller = "Account", action = "Login" }));
            }
        }
    }
}