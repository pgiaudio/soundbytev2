﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace AVStore.Utilities
{
    /// <summary>
    /// Holds File Data in memory for the MultipartFormDataMemoryStreamProvider
    /// </summary>
    public class MemoryStreamFileData
    {
        /// <summary>
        /// The file name of the specified file
        /// </summary>
        public string LocalFileName { get; private set; }
        /// <summary>
        /// The file content
        /// </summary>
        public Stream LocalFileContent { get; private set; }

        /// <summary>
        /// Constructor that creates a new instance
        /// </summary>
        public MemoryStreamFileData(string localFileName, Stream localFileContent)
        {
            if (localFileName == null)
            {
                throw new ArgumentNullException("localFileName");
            }

            if (localFileContent == null)
            {
                throw new ArgumentNullException("localFileContent");
            }

            LocalFileContent = localFileContent;
            LocalFileName = localFileName;
        }
        

    }


    /// <summary>
    /// Manages the parsing of a Multipart Form Message from an API Controller
    /// </summary>
    public class MultipartFormDataMemoryStreamProvider : MultipartStreamProvider
    {
        private NameValueCollection _formData = new NameValueCollection(StringComparer.OrdinalIgnoreCase);

        private Collection<bool> _isFormData = new Collection<bool>();

        private Collection<MemoryStreamFileData> _fileData = new Collection<MemoryStreamFileData>();

        /// <summary>
        /// The Collection of Form Data submitted with the request
        /// </summary>
        public NameValueCollection FormData
        {
            get { return _formData; }
        }

        /// <summary>
        /// The Collection of Files submitted with the request
        /// </summary>
        public Collection<MemoryStreamFileData> FileData
        {
            get { return _fileData; }
        }

        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }

            // For form data, Content-Disposition header is a requirement
            ContentDispositionHeaderValue contentDisposition = headers.ContentDisposition;
            if (contentDisposition != null)
            {
                // If we have a file name then write contents out to AWS stream. Otherwise just write to MemoryStream
                if (!String.IsNullOrEmpty(contentDisposition.FileName))
                {
                    _isFormData.Add(false);      
                }
                else
                {
                    // We will post process this as form data
                    _isFormData.Add(true);
                }

                return new MemoryStream();
            }

            throw new InvalidOperationException("Did not find required 'Content-Disposition' header field in MIME multipart body part..");
        }

        /// <summary>
        /// Read the non-file contents as form data.
        /// </summary>
        /// <returns></returns>
        public override async Task ExecutePostProcessingAsync()
        {
            // Find instances of HttpContent for which we created a memory stream and read them asynchronously
            // to get the string content and then add that as form data
            for (int index = 0; index < Contents.Count; index++)
            {
                if (_isFormData[index])
                {
                    HttpContent formContent = Contents[index];
                    // Extract name from Content-Disposition header. We know from earlier that the header is present.
                    ContentDispositionHeaderValue contentDisposition = formContent.Headers.ContentDisposition;
                    string formFieldName = UnquoteToken(contentDisposition.Name) ?? String.Empty;

                    // Read the contents as string data and add to form data
                    string formFieldValue = await formContent.ReadAsStringAsync();
                    FormData.Add(formFieldName, formFieldValue);
                }
                else
                {
                    HttpContent fileContent = Contents[index];
                    ContentDispositionHeaderValue contentDisposition = fileContent.Headers.ContentDisposition;
                    string fileName = UnquoteToken(contentDisposition.FileName) ?? String.Empty;

                    _fileData.Add(new MemoryStreamFileData(fileName, await fileContent.ReadAsStreamAsync()));
                }
            }
        }

        /// <summary>
        /// Remove bounding quotes on a token if present
        /// </summary>
        /// <param name="token">Token to unquote.</param>
        /// <returns>Unquoted token.</returns>
        private static string UnquoteToken(string token)
        {
            if (String.IsNullOrWhiteSpace(token))
            {
                return token;
            }

            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1)
            {
                return token.Substring(1, token.Length - 2);
            }

            return token;
        }
    }

}