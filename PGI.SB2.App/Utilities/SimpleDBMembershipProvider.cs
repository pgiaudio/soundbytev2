﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using Amazon.SimpleDB.Model;
using Amazon.SimpleDB;
using PGI.SB2.Entity;
using log4net;
using log4net.Config;

namespace PGI.SB2.App.Utilities
{
    public class SimpleDBMembershipProvider : ExtendedMembershipProvider 
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SimpleDBMembershipProvider));

        public SimpleDBMembershipProvider()
        {
            XmlConfigurator.Configure();
        }

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
        }


        public override string CreateUserAndAccount(string userName, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            SB2Domain userDomain = new SB2Domain(Settings.AWSUserDomain);
            string status = MembershipCreateStatus.UserRejected.ToString();
            MembershipUser memUser = this.GetUser(userName, false);

            if (memUser == null)
            {
                User user = new User();
                user.UserName = userName;
                user.Password = password;
                user.Email = values["email"].ToString();

                if (user.Save(userDomain) == true)
                {
                    status = MembershipCreateStatus.Success.ToString();
                }
                else
                {
                    if (user.LastError != null)
                    {
                        status = user.LastError.Message;
                    }
                }
            }

            return status;
        }

        public override ICollection<OAuthAccountData> GetAccountsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            SB2Domain userDomain = new SB2Domain(Settings.AWSUserDomain);
            MembershipUser memUser = null;
            if (!String.IsNullOrEmpty(username))
            {
                User user = new User();

                if (user.Retrieve(userDomain, username))
                {
                    memUser = new MembershipUser(
                        this.Name,
                        user.UserName,
                        String.Empty,
                        user.Email,
                        String.Empty,
                        String.Empty,
                        true,
                        !user.Active,
                        DateTime.Today,
                        DateTime.Today,
                        DateTime.Today,
                        DateTime.Today,
                        DateTime.Today
                        );
                }

            }

            return memUser;
        }

        public override string GetUserNameByEmail(string email)
        {
            SB2Domain userDomain = new SB2Domain(Settings.AWSUserDomain);
            if (!String.IsNullOrEmpty(email))
            {
                List<Item> userList = userDomain.RetrieveItems("Select * from `" + userDomain.DomainName + "` where Email = '" + email + "'");

                if (userList != null && userList.Count > 0)
                {
                    return userList[0].Name;
                }
            }

            return String.Empty;
        }

        public override bool ValidateUser(string userName, string password)
        {
            SB2Domain userDomain = new SB2Domain(Settings.AWSUserDomain);
            User user = new User();
            return user.Login(userDomain, userName, password);
        }

        public override bool ConfirmAccount(string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmAccount(string userName, string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateAccount(string userName, string password, bool requireConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteAccount(string userName)
        {
            throw new NotImplementedException();
        }

        public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetCreateDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetLastPasswordFailureDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetPasswordChangedDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetUserIdFromPasswordResetToken(string token)
        {
            throw new NotImplementedException();
        }

        public override bool IsConfirmed(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool ResetPasswordWithToken(string token, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
    }
}