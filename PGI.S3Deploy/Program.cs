﻿using Amazon.S3;
using Amazon.S3.Model;
using CommandLine;
using CommandLine.Text;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.S3Deploy
{
    // Define a class to receive parsed values
    public class Options
    {
        [Option('d', "dir", Required = true, HelpText = "Directory to files in archive")]
        public string Directory { get; set; }

        [Option('p', "pat", Required = true, HelpText = "Pattern to use to match files to include in archive.")]
        public string Pattern { get; set; }

        [Option('k', "pkg", Required = true, HelpText = "root path to use ")]
        public string Package { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }


    class Program
    {
        enum ExitCode : int
        {
            Success = 0,
            Error = 1
        }

        static void Main(string[] args)
        {
            try
            {
                foreach (var arg in args)
                {
                    Console.WriteLine("Arguments: " + arg);
                }

                var options = new Options();
                CommandLine.Parser.Default.ParseArguments(args, options);

                //string directory = cmdline["dir"];
                //if (string.IsNullOrEmpty(directory))
                //{
                //    Console.WriteLine("/dir parameter must be specified");
                //    Environment.Exit((int)ExitCode.Error);
                //}

                //string pattern = cmdline["pat"];
                //if (string.IsNullOrEmpty(pattern))
                //{
                //    Console.WriteLine("/pat parameter must be specified");
                //    Environment.Exit((int)ExitCode.Error);
                //}

                //string packageName = cmdline["pkg"];
                //if (string.IsNullOrEmpty(pattern))
                //{
                //    Console.WriteLine("/pkg parameter must be specified");
                //    Environment.Exit((int)ExitCode.Error);
                //}


                string[] fileList = Directory.GetFiles(options.Directory, options.Pattern);
                
                if (fileList.Length > 0)
                {
                    Console.WriteLine("The number of matched files is {0}.", fileList.Length);
                    using (ZipFile zip = new ZipFile())
                    {

                        foreach (string file in fileList)
                        {
                            zip.AddFile(file, "");
                            Console.WriteLine("Adding file {0} to archive.", file);
                        }

                        //Save as latest
                        using (var ms = new MemoryStream())
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            zip.Save(ms);

                            Console.WriteLine("Saving Archive");
                            AddObjectToS3(options.Package, ms);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No files found for the specified parameters");
                    Environment.Exit((int)ExitCode.Error);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Environment.Exit((int)ExitCode.Error);
            }

            Environment.Exit((int)ExitCode.Success);
        }


        public static void AddObjectToS3(string PackageName, Stream FileStream)
        {

            Amazon.RegionEndpoint BucketRegion = GetRegionByName(GetSetting("S3BucketRegion"));
            Console.WriteLine("Amazon Region is " + BucketRegion.DisplayName);
            Console.WriteLine("S3 Bucket Name is " + BucketRegion.DisplayName);
            IAmazonS3 S3Client = new AmazonS3Client(BucketRegion);

            PutObjectRequest putObjectRequest = new PutObjectRequest();
            putObjectRequest.BucketName = GetSetting("S3BucketName");
            putObjectRequest.CannedACL = S3CannedACL.PublicRead;
            putObjectRequest.Key = PackageName + "-latest.zip";
            putObjectRequest.InputStream = FileStream;
            PutObjectResponse putObjectResponse = S3Client.PutObject(putObjectRequest);

            Console.WriteLine("Package {0} Added to {1}", putObjectRequest.Key, putObjectRequest.BucketName);

            CopyObjectRequest copyObjectRequest = new CopyObjectRequest();
            copyObjectRequest.SourceBucket = GetSetting("S3BucketName");
            copyObjectRequest.DestinationBucket = GetSetting("S3BucketName");
            copyObjectRequest.CannedACL = S3CannedACL.PublicRead;
            copyObjectRequest.SourceKey = PackageName + "-latest.zip";
            copyObjectRequest.DestinationKey = PackageName + "-" + System.DateTime.Now.ToString("yyyyMMddmmss") + ".zip";
            S3Client.CopyObject(copyObjectRequest);

            Console.WriteLine("Package {0} Copied to {1} in {2}", copyObjectRequest.SourceKey, copyObjectRequest.DestinationKey, copyObjectRequest.DestinationBucket);
        }

        public static Amazon.RegionEndpoint GetRegionByName(string RegionName)
        {
            switch (RegionName.ToLower())
            {
                case "us-east-1":
                    return Amazon.RegionEndpoint.USEast1;
                case "us-west-1":
                    return Amazon.RegionEndpoint.USWest1;
                case "us-west-2":
                    return Amazon.RegionEndpoint.USWest2;
                case "eu-west-1":
                    return Amazon.RegionEndpoint.EUWest1;
                case "ap-northeast-1":
                    return Amazon.RegionEndpoint.APNortheast1;
                case "ap-southeast-1":
                    return Amazon.RegionEndpoint.APSoutheast1;
                case "ap-southeast-2":
                    return Amazon.RegionEndpoint.APSoutheast2;
                case "sa-east-1":
                    return Amazon.RegionEndpoint.SAEast1;
            }

            return null;
        }

        public static string GetSetting(string key)
        {
            if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings[key]))
                return System.Configuration.ConfigurationManager.AppSettings[key];
            else
                return "";
        }

    }
}
