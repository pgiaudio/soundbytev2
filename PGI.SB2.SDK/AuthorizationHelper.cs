﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    public static class AuthorizationHelper
    {
        public const string UTC_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";

        public class ParameterComparer : IComparer<Parameter>
        {
            public int Compare(Parameter x, Parameter y)
            {
                return x.Name.CompareTo(y.Name);
            }
        }

        private static string ComputeHash(string SecretKey, string message)
        {
            var key = Encoding.UTF8.GetBytes(SecretKey.ToUpper());
            string hashString;

            using (var hmac = new HMACSHA256(key))
            {
                var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));
                hashString = Convert.ToBase64String(hash);
            }

            return hashString;
        }

        /// <summary>
        /// Creates an HMAC Authorization Header for a given request
        /// </summary>
        /// <param name="AccessKey">The Access Key for the specified account</param> 
        /// <param name="SecretKey">The Secret Key for the specifid account</param>
        /// <param name="RequestDate">The Date of the request</param>
        /// <param name="RequestVerb">The Date of the request</param>
        /// <param name="UserName">The name of the user making this request</param>
        /// <returns>Retruns an Authorization Header string that can be used to make an API request</returns>
        /// 
        public static string CreateAuthHeaderString(string AccessKey, string SecretKey, System.DateTime RequestDate)
        {
            string hash = ComputeHash(SecretKey, "");
            StringBuilder sb = new StringBuilder();

            sb.Append("HMAC ");
            sb.Append(AccessKey);
            sb.Append(":");
            sb.Append(hash);

            return sb.ToString();
        }

 

    }
}
