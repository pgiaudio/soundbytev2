﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    public class PluginDescription
    {
        public string Name { get; set; }
        public int Identifier { get; set; }
        public string AssemblyName { get; set; }
    }

    /// <summary>
    /// The Session Object for Read Requests
    /// </summary>
    public class ListPlugins
    {
        public List<PluginDescription> Plugins { get; set; }

        public ListPlugins()
        {
            Plugins = new List<PluginDescription>();
        }
    }

    /// <summary>
    /// The Request Object for List Plugins Requests
    /// </summary>
    public class ListPluginsRequest : APIRequest
    {

    }

    /// <summary>
    /// The Response Object for List Plugins Requests
    /// </summary>
    public class ListPluginsResponse : APIResponse
    {
        /// <summary>
        /// An instance of the ListPlugins
        /// </summary>
        public ListPlugins Detail { get; set; }
    }
}
