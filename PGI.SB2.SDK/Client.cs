﻿using log4net;
using log4net.Config;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    public static class Client
    {
        
        private static readonly ILog log = LogManager.GetLogger(typeof(Client));
        private static string _namespace = typeof(Client).Namespace;

        /// <summary>
        /// Read an existing Asset from the repository
        /// </summary>
        /// <param name="Request">An object that encapsulates the parameters needed to invoke the request</param> 
        /// <returns>A response object that contains the Asset information</returns>
        public static ReadAssetResponse ReadAsset(ReadAssetRequest Request)
        {
            ReadAssetResponse response = new ReadAssetResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/asset/read/" + Request.AssetId.ToString(), Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                if (!string.IsNullOrEmpty(Request.VersionId))
                {
                    restRequest.AddParameter("VersionID", Request.VersionId);
                }

                IRestResponse<ReadAsset> restResponse = client.Execute<ReadAsset>(restRequest);
                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Detail = restResponse.Data;
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".ReadAsset", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Create a new Asset and add it to the Repository.
        /// </summary>
        /// <param name="Request">An object that encapsulates the data associated with the request</param> 
        /// <param name="FileStream">The content of the file</param>
        /// <returns>A response object that contains the ID of the newly created Asset</returns>
        public static CreateAssetResponse CreateAsset(CreateAssetRequest Request)
        {
            CreateAssetResponse response = new CreateAssetResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/asset/create/", Method.POST);
                restRequest.RequestFormat = DataFormat.Json;
                restRequest.AddParameter("Name", Request.Asset.Name);
                restRequest.AddParameter("CompanyID", Request.Asset.CompanyID);
                restRequest.AddParameter("CompanyName", Request.Asset.CompanyName);
                restRequest.AddParameter("ClientID", Request.Asset.ClientID);
                restRequest.AddParameter("ModeratorName", Request.Asset.ModeratorName);
                restRequest.AddParameter("ModeratorEmail", Request.Asset.ModeratorEmail);
                restRequest.AddParameter("ConferenceID", Request.Asset.ConferenceID);
                restRequest.AddParameter("ConferenceName", Request.Asset.ConferenceName);
                restRequest.AddParameter("ConferenceLink", Request.Asset.ConferenceLink);
                restRequest.AddParameter("EventStartUTC", Request.Asset.EventStartUTC);
                restRequest.AddParameter("EventEndUTC", Request.Asset.EventEndUTC);
                restRequest.AddParameter("Notes", Request.Asset.Notes);
                restRequest.AddParameter("RegionName", Request.Asset.RegionName);

                //Get the file stream
                
                using(var memoryStream = new MemoryStream())
                {
                    Request.InputStream.CopyTo(memoryStream);
                    byte[]  fileContent = memoryStream.ToArray();

                    restRequest.AddFile("FileData", fileContent, Request.FileName);
                }

                IRestResponse<System.Guid> restResponse = client.Execute<System.Guid>(restRequest);

                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.AssetID = new System.Guid(restResponse.Content.Replace("\"", ""));
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".CreateAsset", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }


        public static UpdateAssetResponse AddVersion(UpdateAssetRequest Request)
        {
            UpdateAssetResponse response = new UpdateAssetResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/asset/addversion/", Method.PUT);
                restRequest.RequestFormat = DataFormat.Json;
                restRequest.AddParameter("AssetID", Request.AssetID);
                restRequest.AddParameter("Notes", Request.Notes);

                //Get the file stream

                using (var memoryStream = new MemoryStream())
                {
                    Request.InputStream.CopyTo(memoryStream);
                    byte[] fileContent = memoryStream.ToArray();

                    restRequest.AddFile("FileData", fileContent, Request.FileName);
                }

                IRestResponse<System.Guid> restResponse = client.Execute<System.Guid>(restRequest);

                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.AssetID = Request.AssetID;
                    response.VersionID = restResponse.Content.Replace("\"", "");
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".CreateAsset", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Read an existing Asset from the repository
        /// </summary>
        /// <param name="Request">An object that encapsulates the parameters needed to invoke the request</param> 
        /// <returns>A response object that contains the Asset information</returns>
        public static RunPluginsResponse RunPlugins(RunPluginsRequest Request)
        {
            RunPluginsResponse response = new RunPluginsResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                var restRequest = new RestRequest("api/asset/runplugins/", Method.PUT);
                restRequest.RequestFormat = DataFormat.Json;
                restRequest.AddParameter("AssetID", Request.AssetID);
                if (!string.IsNullOrEmpty(Request.VersionID))
                {
                    restRequest.AddParameter("VersionID", Request.VersionID);
                }

                IRestResponse<bool> restResponse = client.Execute<bool>(restRequest);
                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.RunResult = restResponse.Data;
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".RunPlugins", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }




        /// <summary>
        /// Read an existing Asset from the repository
        /// </summary>
        /// <param name="Request">An object that encapsulates the parameters needed to invoke the request</param> 
        /// <returns>A response object that contains the Search information</returns>
        public static SearchAssetsResponse SearchAssets(SearchAssetsRequest Request)
        {
            SearchAssetsResponse response = new SearchAssetsResponse();
            response.Phrase = Request.Phrase;

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/asset/search/", Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                if (!string.IsNullOrEmpty(Request.Phrase))
                {
                    restRequest.AddParameter("Phrase", Request.Phrase);
                    restRequest.AddParameter("CurrentPage", Request.CurrentPage);
                    restRequest.AddParameter("ResultsPerPage", Request.ResultsPerPage);
                }

                IRestResponse<AssetSearchDetail> restResponse = client.Execute<AssetSearchDetail>(restRequest);
                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Detail = restResponse.Data;
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".SearchAssets", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Read an existing Session from the repository
        /// </summary>
        /// <param name="Request">An object that encapsulates the parameters needed to invoke the request</param> 
        /// <returns>A response object that contains the Session information</returns>
        public static ReadSessionResponse ReadSession(ReadSessionRequest Request)
        {
            ReadSessionResponse response = new ReadSessionResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/session/read/" + Request.SessionId.ToString(), Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                IRestResponse<ReadSession> restResponse = client.Execute<ReadSession>(restRequest);
                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Detail = restResponse.Data;
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".ReadSession", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Create a new Session for the Management Console.
        /// </summary>
        /// <param name="Request">An object that encapsulates the data associated with the request</param> 
        /// <returns>A response object that contains the ID of the newly created Session</returns>
        public static CreateSessionResponse CreateSession(CreateSessionRequest Request)
        {
            CreateSessionResponse response = new CreateSessionResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/session/create/", Method.POST);
                restRequest.RequestFormat = DataFormat.Json;
                restRequest.AddParameter("UserName", Request.UserName);
                restRequest.AddParameter("Password", Request.Password);

                //Get the file stream

                IRestResponse<System.Guid> restResponse = client.Execute<System.Guid>(restRequest);

                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.SessionID = new System.Guid(restResponse.Content.Replace("\"", ""));
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".CreateSession", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }




        /// <summary>
        /// Delete a Session for the Management Console.
        /// </summary>
        /// <param name="Request">An object that encapsulates the data associated with the request</param> 
        /// <returns>A response object that contains the ID of the newly created Session</returns>
        public static DeleteSessionResponse DeleteSession(DeleteSessionRequest Request)
        {
            DeleteSessionResponse response = new DeleteSessionResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //We Add the ID to the URL because we do not want the ID being included in the list of parameters
                var restRequest = new RestRequest("api/session/delete/", Method.POST);
                restRequest.RequestFormat = DataFormat.Json;
                restRequest.AddParameter("SessionId", Request.SessionId);

                //Get the file stream

                IRestResponse<bool> restResponse = client.Execute<bool>(restRequest);

                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Result = Convert.ToBoolean((restResponse.Content.Replace("\"", "")));
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".DeleteSession", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }



        /// <summary>
        /// Read an existing Session from the repository
        /// </summary>
        /// <param name="Request">An object that encapsulates the parameters needed to invoke the request</param> 
        /// <returns>A response object that contains the Session information</returns>
        public static ListPluginsResponse ListPlugins(ListPluginsRequest Request)
        {
            ListPluginsResponse response = new ListPluginsResponse();

            try
            {
                var client = new RestClient(Request.BaseURL);
                client.Authenticator = new HMACAuthenticator(Request.AccessKey, Request.SecretKey);

                //No Additional Parameters are required for this request
                var restRequest = new RestRequest("api/plugin/list", Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                IRestResponse<ListPlugins> restResponse = client.Execute<ListPlugins>(restRequest);
                response.StatusCode = Convert.ToInt32(restResponse.StatusCode);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Detail = restResponse.Data;
                }
                else
                {
                    response.Message = restResponse.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                log.Error(_namespace + ".ListPlugins", ex);

                response.StatusCode = 500;
                response.Message = ex.Message;
            }

            return response;
        }




    }
}

/*
// execute the request
RestResponse response = client.Execute(request);
var content = response.Content; // raw content as string

// or automatically deserialize result
// return content type is sniffed but can be explicitly set via RestClient.AddHandler();
            
var name = response2.Data.Name;

// easy async support
client.ExecuteAsync(request, response =>
{
    Console.WriteLine(response.Content);
});

// async with deserialization
var asyncHandle = client.ExecuteAsync<Person>(request, response =>
{
    Console.WriteLine(response.Data.Name);
});

// abort the request on demand
asyncHandle.Abort();
*/
