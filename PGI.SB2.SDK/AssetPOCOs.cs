﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    /// <summary>
    /// The Asset Object for Read Requests
    /// </summary>
    public class ReadAsset
    {
        /// <summary>
        /// The Asset Identifier
        /// </summary>
        public string AssetID { get; set; }
        /// <summary>
        /// The most recent version identifier for this asset
        /// </summary>
        public string VersionID { get; set; }
        /// <summary>
        /// The name of the asset
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The ID of the company that this asset belongs to
        /// </summary>
        public string CompanyID { get; set; }
        /// <summary>
        /// The Name of the company that this asset belongs to
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// The name of the ClientID for the Moderator that this asset belongs to
        /// </summary>
        public string ClientID { get; set; }
        /// <summary>
        /// The name of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorName { get; set; }
        /// <summary>
        /// The Email Address of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorEmail { get; set; }
        /// <summary>
        /// The AWS Region where this asset is stored
        /// </summary>
        public string RegionName { get; set; }
        /// <summary>
        /// The Conference Link (if applicable) for this asset
        /// </summary>
        public string ConferenceLink { get; set; }
        /// <summary>
        /// The Conference ID (if applicable) for this asset
        /// </summary>
        public string ConferenceID { get; set; }
        /// <summary>
        /// The Conference Name (if applicable) for this asset
        /// </summary>
        public string ConferenceName { get; set; }
        /// <summary>
        /// The Event Start Time for the conference associated with this asset in UTC
        /// </summary>
        public System.DateTime EventStartUTC { get; set; }
        /// <summary>
        /// The Event End Time for the conference associated with this asset in UTC
        /// </summary>
        public System.DateTime EventEndUTC { get; set; }
        /// <summary>
        /// The filename of the source file for this asset
        /// </summary>
        public string SourceFileName { get; set; }
        /// <summary>
        /// The file extension for the source file for this asset
        /// </summary>
        public string SourceFileExtension { get; set; }
        /// <summary>
        /// The most recent version identifier for this asset
        /// </summary>
        public string CurrentVersionID { get; set; }
        /// <summary>
        /// A numberical value for the most recent version for this asset
        /// </summary>
        public int CurrentVersionNumber { get; set; }
        /// <summary>
        /// Additional information about this asset
        /// </summary>
        
        public AssetVersion CurrentVersion
        {
            get
            {
                if (this.Versions != null)
                {
                    return this.Versions.FirstOrDefault(v => v.VersionID == CurrentVersionID);
                }
                else
                {
                    return null;
                }
            }
        }
        public string Notes { get; set; }
        /// <summary>
        /// The URL to download the Asset from AWS
        /// </summary>
        public string DownloadURL { get; set; }
        /// <summary>
        /// A collection of different versions of the asset
        /// </summary>
        public List<PGI.SB2.SDK.AssetVersion> Versions { get; set; }
    }

    /// <summary>
    /// The Request Object for Asset Read Requests
    /// </summary>
    public class ReadAssetRequest : APIRequest
    {
        /// <summary>
        /// The ID of the asset to retrieve
        /// </summary>
        public System.Guid AssetId { get; set; }
        /// <summary>
        /// The optional version to retreive, if left blank, the current version will be read
        /// </summary>
        public string VersionId { get; set; }
    }

    /// <summary>
    /// The Response Object for Asset Read Requests
    /// </summary>
    public class ReadAssetResponse : APIResponse
    {
        /// <summary>
        /// An instance of the Asset Object for Read Requests
        /// </summary>
        public ReadAsset Detail { get; set; }
    }

    /// <summary>
    /// The Asset Object for Create Requests
    /// </summary>
    public class CreateAsset
    {
        /// <summary>
        /// The name of the asset
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The ID of the company that this asset belongs to
        /// </summary>
        public string CompanyID { get; set; }
        /// <summary>
        /// The Name of the company that this asset belongs to
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// The name of the ClientID for the Moderator that this asset belongs to
        /// </summary>
        public string ClientID { get; set; }
        /// <summary>
        /// The name of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorName { get; set; }
        /// <summary>
        /// The Email Address of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorEmail { get; set; }
        /// <summary>
        /// The Conference ID (if applicable) for this asset
        /// </summary>
        public string ConferenceID { get; set; }
        /// <summary>
        /// The Conference Name (if applicable) for this asset
        /// </summary>
        public string ConferenceName { get; set; }
        /// <summary>
        /// The Conference Link (if applicable) for this asset
        /// </summary>
        public string ConferenceLink { get; set; }
        /// <summary>
        /// The Event Start Time for the conference associated with this asset in UTC
        /// </summary>
        public string EventStartUTC { get; set; }
        /// <summary>
        /// The Event End Time for the conference associated with this asset in UTC
        /// </summary>
        public string EventEndUTC { get; set; }
        /// <summary>
        /// The Region Name where this asset should be stored
        /// </summary>
        public string RegionName { get; set; }
        /// <summary>
        /// Additional information about this asset
        /// </summary>
        public string Notes { get; set; }
    }

    /// <summary>
    /// The Request Object for Creating an Asset
    /// </summary>
    public class CreateAssetRequest : APIRequest
    {
        /// <summary>
        /// An instance of the Asset Object for Create Requests
        /// </summary>
        public CreateAsset Asset { get; set; }
        /// <summary>
        /// The file name of the Asset
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// The contents of the asset
        /// </summary>
        public System.IO.Stream InputStream { get; set; }
    }

    /// <summary>
    /// The Response Object for Creating an Asset
    /// </summary>
    public class CreateAssetResponse : APIResponse
    {
        /// <summary>
        /// The ID of the new asset
        /// </summary>
        public System.Guid AssetID { get; set; }
    }


    /// <summary>
    /// The Request Object for Creating an Asset
    /// </summary>
    public class UpdateAssetRequest : APIRequest
    {
        /// <summary>
        /// The ID of the asset to update
        /// </summary>
        public System.Guid AssetID { get; set; }
        /// <summary>
        /// Additional information about this version of the asset
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// The file name of the Asset
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// The contents of the asset
        /// </summary>
        public System.IO.Stream InputStream { get; set; }
    }


    /// <summary>
    /// The Response Object for Updating an Asset
    /// </summary>
    public class UpdateAssetResponse : APIResponse
    {
        /// <summary>
        /// The ID of the new asset
        /// </summary>
        public System.Guid AssetID { get; set; }

        /// <summary>
        /// The Version ID for the new version of the new asset
        /// </summary>
        public string VersionID { get; set; }
    }

    /// <summary>
    /// The Request Object for Running Plugins
    /// </summary>
    public class RunPluginsRequest : APIRequest
    {
        /// <summary>
        /// The ID of the asset to update
        /// </summary>
        public System.Guid AssetID { get; set; }

        /// <summary>
        /// The VersionID of the asset to update
        /// </summary>
        public string VersionID { get; set; }
    }

    /// <summary>
    /// The Response Object for Running Plugins
    /// </summary>
    public class RunPluginsResponse : APIResponse
    {
        public bool RunResult { get; set; }

    }

    public class AssetPluginResult
    {
        public int Id { get; set; }
        public System.DateTime RunAt { get; set; }
        /// <summary>
        /// Number of attempts to run this plugin
        /// </summary>
        public int Att { get; set; }
        public bool Result { get; set; }
    }

    public class AssetVersion
    {
        public class AssetVersionComparer : IComparer<AssetVersion>
        {
            public int Compare(AssetVersion x, AssetVersion y)
            {
                return x.VersionNumber < y.VersionNumber ? 1 : (x.VersionNumber == y.VersionNumber ? 0 : -1);
            }
        }

        /// <summary>
        /// The version Identifier for this asset version
        /// </summary>
        public string VersionID { get; set; }

        /// <summary>
        /// The physical location where the asset is stored
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// The S3 bucket where the asset is stored
        /// </summary>
        public string BucketName { get; set; }

        /// <summary>
        /// The filename of the source file for this version of the asset
        /// </summary>
        public string SourceFileName { get; set; }

        /// <summary>
        /// The file extension for the source file for this version of the asset
        /// </summary>
        public string SourceFileExtension { get; set; }

        /// <summary>
        /// Additional information about this asset version
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// A numerical indicator of the asset version
        /// </summary>
        public int VersionNumber { get; set; }

        /// <summary>
        /// The URL to download the Asset from AWS
        /// </summary>
        public string DownloadURL { get; set; }

        /// <summary>
        /// The user that originally created this asset version
        /// </summary>
        public string CreateUser { get; set; }

        /// <summary>
        /// The date and time that the asset version was originally created
        /// </summary>
        public System.DateTime CreateDateUTC { get; set; }

        /// <summary>
        /// The audit trail for the plugins run against this asset version
        /// </summary>
        public List<AssetPluginResult> PluginResults { get; set; }

        /// <summary>
        /// The current Error Message for the plugin
        /// </summary>
        public string PluginErrorMessage { get; set; }
    }



    /// <summary>
    /// The Response Object for Searching Assets
    /// </summary>
    public class AssetSearchResult
    {
        /// <summary>
        /// The ID of the asset
        /// </summary>
        public string AssetID { get; set; }
        /// <summary>
        /// The name of the asset
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The ID of the company that this asset belongs to
        /// </summary>
        public string CompanyID { get; set; }
        /// <summary>
        /// The Name of the company that this asset belongs to
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// The name of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorName { get; set; }
        /// <summary>
        /// The Email Address of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorEmail { get; set; }
        /// <summary>
        /// The Conference ID (if applicable) for this asset
        /// </summary>
        public string ConferenceID { get; set; }
        /// <summary>
        /// The Conference Name (if applicable) for this asset
        /// </summary>
        public string ConferenceName { get; set; }
        /// <summary>
        /// The Region Name where this asset should be stored
        /// </summary>
        public string RegionName { get; set; }
        /// <summary>
        /// A numerical indicator of the asset version
        /// </summary>
        public int VersionCount { get; set; }
        /// <summary>
        /// The filename of the source file for this asset
        /// </summary>
        public string SourceFileName { get; set; }
        /// <summary>
        /// The file extension for the source file for this asset
        /// </summary>
        public string SourceFileExtension { get; set; }
        /// <summary>
        /// Additional information about this asset
        /// </summary>
        public string Notes { get; set; }
    }

    public class AssetSearchDetail
    {
        public int ResultCount { get; set; }
        public int ResultsPerPage { get; set; }
        public int PageCount { get; set; }
        public List<AssetSearchResult> Results { get; set; }

        public AssetSearchDetail()
        {
            Results = new List<AssetSearchResult>();
        }
    }


    /// <summary>
    /// The Request Object for Searching Assets
    /// </summary>
    public class SearchAssetsRequest : APIRequest
    {
        /// <summary>
        /// The phrase to use when searching the assets
        /// </summary>
        public string Phrase { get; set; }
        public int CurrentPage { get; set; }
        public int ResultsPerPage { get; set; }
    }

    /// <summary>
    /// The Response Object for Searching Assets
    /// </summary>
    public class SearchAssetsResponse : APIResponse
    {
        public string Phrase { get; set; }
        public AssetSearchDetail Detail { get; set; }
    }



}
