﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    /// <summary>
    /// The Session Object for Read Requests
    /// </summary>
    public class ReadSession
    {
        /// <summary>
        /// The Session Identifier
        /// </summary>
        public System.Guid ID { get; set; }
        /// <summary>
        /// The Time that the Session Expires at
        /// </summary>
        public System.DateTime Expiration { get; set; }
        /// <summary>
        /// The Users Role
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// The Username associated with this session
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// The Secret Key for the user associated with this session
        /// </summary>
        public string SecretKey { get; set; }
    }


    /// <summary>
    /// The Request Object for Session Read Requests
    /// </summary>
    public class ReadSessionRequest : APIRequest
    {
        /// <summary>
        /// The ID of the session to retrieve
        /// </summary>
        public System.Guid SessionId { get; set; }
    }

    /// <summary>
    /// The Response Object for Session Read Requests
    /// </summary>
    public class ReadSessionResponse : APIResponse
    {
        /// <summary>
        /// An instance of the Session Object for Read Requests
        /// </summary>
        public ReadSession Detail { get; set; }
    }


    /// <summary>
    /// The Request Object for Asset Read Requests
    /// </summary>
    public class CreateSessionRequest : APIRequest
    {
        /// <summary>
        /// The username for the user
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// The password for the user
        /// </summary>
        public string Password { get; set; }
    }

    /// <summary>
    /// The Response Object for Creating an Session
    /// </summary>
    public class CreateSessionResponse : APIResponse
    {
        /// <summary>
        /// The ID of the new asset
        /// </summary>
        public System.Guid SessionID { get; set; }
    }

    /// <summary>
    /// The Request Object for Session Delete Requests
    /// </summary>
    public class DeleteSessionRequest : APIRequest
    {
        /// <summary>
        /// The ID of the session to retrieve
        /// </summary>
        public System.Guid SessionId { get; set; }
    }

    /// <summary>
    /// The Response Object for Session Delete Requests
    /// </summary>
    public class DeleteSessionResponse : APIResponse
    {
        /// <summary>
        /// An instance of the Session Object for Read Requests
        /// </summary>
        public bool Result { get; set; }
    }
}
