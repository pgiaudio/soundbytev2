﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    /// <summary>
    /// This is an implementation of an Authenticator for Rest Sharp that uses HMAC authentication
    /// HMAC authentication generates a hash based on the contents of the message being sent, that 
    /// is encrypted by a private key to create a hash.  When the message is received by the server
    /// the key is generated on the server again, based on information provided in the message, and if 
    /// successful, the message is authenticated.
    /// </summary>
    public class HMACAuthenticator : IAuthenticator
    {
        /// <summary>
        /// A class to sort parameters of a rest request alphabetically
        /// </summary>
        public class ParameterComparer : IComparer<Parameter>
        {
            public int Compare(Parameter x, Parameter y)
            {
                return x.Name.CompareTo(y.Name);
            }
        }

        public const string UTC_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";

        public string AccessKey { get; private set; }
        public string SecretKey { get; private set; }

        /// <summary>
        /// Constructor to create a new instance of this Authenticator
        /// </summary>
        public HMACAuthenticator (string AccessKey, string SecretKey)
        {
            this.AccessKey = AccessKey;
            this.SecretKey = SecretKey;
        }

        /// <summary>
        /// Authenticate
        /// </summary>
        /// <param name="client">The rest sharp client instance</param> 
        /// <param name="request">The details about the specific request</param>
        /// <returns>void</returns>
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            System.DateTime Timestamp = System.DateTime.Now.ToUniversalTime();
            request.AddParameter("Timestamp", Timestamp.ToString(UTC_FORMAT));

            request.Parameters.Sort(new ParameterComparer());
            
            StringBuilder sb = new StringBuilder();
            sb.Append(request.Method.ToString());
            foreach(var reqParam in request.Parameters)
            {
                sb.Append("&" + reqParam.Name + "=" + reqParam.Value);
            }

            request.AddHeader("Authorization", "HMAC " + AccessKey + ":" + ComputeHash(SecretKey, sb.ToString()));
        }

        /// <summary>
        /// Creates a hash for a given message and secret key
        /// </summary>
        /// <param name="SecretKey">The secret key to use to encrypt the message</param> 
        /// <param name="Message">The message to encrypt</param>
        /// <returns>an encrypted hash</returns>
        private string ComputeHash(string SecretKey, string message)
        {
            var key = Encoding.UTF8.GetBytes(SecretKey.ToUpper());
            string hashString;

            using (var hmac = new HMACSHA256(key))
            {
                var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));
                hashString = Convert.ToBase64String(hash);
            }

            return hashString;
        }
    }
}
