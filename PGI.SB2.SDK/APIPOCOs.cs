﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.SDK
{
    public class APIRequest
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string BaseURL { get; set; }
    }

    public class APIResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
