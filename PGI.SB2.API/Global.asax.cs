﻿using PGI.SB2.API.Utilities;
using PGI.SB2.Plugins;
using PGI.SB2.SDK;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PGI.SB2.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            InitializeIOCConatainer();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Formatters.Add(new FileMediaFormatter());

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void InitializeIOCConatainer()
        {
            string path = HttpContext.Current.Server.MapPath("~");
            ObjectFactory.Initialize(x =>
            {
                //Automatically Add the SendEmail Component to the plugins for StructureMap
                //x.For<IAssetEvent>().Use<SendEmailNotification>();
                x.Scan(scanner =>
                {
                    //scanner.AssembliesFromPath(HttpContext.Current.Server.MapPath("~/Bin"));
                    //scanner.AssembliesFromPath(HttpContext.Current.Server.MapPath("~/Extensions"));
                    //scanner.LookForRegistries();
                    scanner.AssemblyContainingType<IAssetEvent>();
                    //scanner.With(new SingletonConvention<IAssetEvent>());
                    scanner.AddAllTypesOf<IAssetEvent>();
                });
            });
        }


    }
}
