﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using log4net.Config;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Text;
using PGI.SB2.Core;
using System.Threading.Tasks;
using System.Threading;
using PGI.SB2.Entity;

namespace PGI.SB2.API.Utilities
{
    public class HMACAuthorization
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HMACAuthorization));

        public string AccessKey { get; private set; }
        public string RequestHash { get; private set; }
        public string SecretKey { get; private set; }

        public HMACAuthorization()
        {
            XmlConfigurator.Configure();
        }

        /// <summary>
        ///     Validate the Request against its Authorization Header, due to limitations in the model binding of WebAPI
        ///     this method must be called inline for each API call, it cannot use the Authroization Attribute.  For Get Messages
        ///     The following  method HttpUtility.ParseQueryString(Request.RequestUri.Query) will convert the query string into 
        ///     a NameValueCollection,  for most put/post requests, the FormData can be retrieved using Request.Content.ReadAsFormDataAsync().Result;
        /// </summary>
        /// <param name="Request">The Request Message</param>
        /// <param name="Parameters">The Parameters associated with the request</param>
        /// <param name="AllowedRoles">A pipe delimited list of roles that are allowed for this api method, use "All" for all roles</param>
        public void Authorize(HttpRequestMessage Request, NameValueCollection Parameters, string AllowedRoles)
        {
            string AuthHeader = Request.Headers.GetValues("Authorization").FirstOrDefault();

            if (!string.IsNullOrEmpty(AuthHeader))
            {
                string pattern = @"HMAC (?<AccessKey>.*?):(?<RequestHash>.*)";
                Match headerMatch = Regex.Match(AuthHeader, pattern);

                AccessKey = headerMatch.Groups["AccessKey"].Value;
                RequestHash = headerMatch.Groups["RequestHash"].Value;

                //TODO: Need to add code to lookup secret key from provided accessKey

                SB2Domain userDomain = new SB2Domain(Settings.AWSUserDomain);
                User user = new User();

                if (user.Retrieve(userDomain, AccessKey.ToLower()))
                {
                    SecretKey = user.Password;

                    StringBuilder sb = new StringBuilder();
                    sb.Append(Request.Method.ToString());

                    System.DateTime timestamp = DateTime.MinValue;
                    foreach (var item in Parameters.AllKeys.OrderBy(k => k))
                    {
                        string itemValue = HttpUtility.UrlDecode(Parameters[item]);
                        sb.Append("&" + item + "=" + itemValue);
                        if (item.ToUpper() == "TIMESTAMP")
                        {
                            timestamp = Convert.ToDateTime(itemValue);
                        }
                    }

                    //Requests are only good for 15 minutes,  so after that time, expire the request
                    TimeSpan ts = System.DateTime.UtcNow - timestamp;
                    if (ts.Minutes > 15)
                    {
                        throw new ApplicationException("The current request has expired");
                    }

                    //Check to see if the generated hash matches what was sent to the server
                    string generatedHash = Utility.ComputeHash(SecretKey, sb.ToString());
                    if (generatedHash == RequestHash)
                    {
                        //
                        var allowedRoles = AllowedRoles.Split('|');
                        if (AllowedRoles.ToLower() == "all" ||
                            allowedRoles.Contains(user.Role) == true)
                        {
                            return;
                        }
                        else
                        {
                            throw new ApplicationException("The user does not have permission to make this request");
                        }
                    }
                }
                else
                {
                    throw new ApplicationException("The specified user does not exist");
                }
            }

            throw new ApplicationException("Invalid authorization credentials");
        }
    }
}