﻿using PGI.SB2.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace PGI.SB2.API.Utilities
{
    /// <summary>
    /// HMAC authentication filter for ASP.NET Web API  This is currently deprecated since the current version of WebAPI
    /// does not adequately support model binding for Multipart form data, which makes it impossible to construct the authorization
    /// header at the time that AuthorizeAttribute is invoked. Currently using the HMACAuthorization instead.
    /// </summary>
    public class HMACAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        public class HMACAuthorizationError
        {
            public string Message { get; set; }
        }

        /// <summary>
        /// Make sure that the correct Authorization Header is included in the request
        /// </summary>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                NameValueCollection paramData;
                string AuthHeader = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();

                if (!string.IsNullOrEmpty(AuthHeader))
                {
                    string pattern = @"HMAC (?<AccessKey>.*?):(?<RequestHash>.*)";
                    Match headerMatch = Regex.Match(AuthHeader, pattern);

                    string accessKey = headerMatch.Groups["AccessKey"].Value;
                    string requestHash = headerMatch.Groups["RequestHash"].Value;

                    //TODO: Need to add code to lookup secret key from provided accessKey
                    string secretKey = "sehSgVirnU7PprHAgeSq+DOUkxhLthP6zF4dRHy2AQs=";

                    StringBuilder sb = new StringBuilder();
                    sb.Append(actionContext.Request.Method.ToString());

                    if (actionContext.Request.Method == HttpMethod.Post ||
                        actionContext.Request.Method == HttpMethod.Put)
                    {
                        //OK, what is happening here is that WebAPI does not natively handle multipart/form-data
                        //messages very well,  so we have to manually extract the form data when working with messages
                        //of this type.  If it is just a regular post of data,  than we can use the typical method to access it.

                        if (actionContext.Request.Content.Headers.ContentType.MediaType.ToLower() == "multipart/form-data")
                        {
                            var parts = actionContext.Request.Content.ReadAsMultipartAsync().Result;

                            paramData = new NameValueCollection(StringComparer.OrdinalIgnoreCase);
                            foreach (var part in parts.Contents.Where(x => x.Headers.ContentDisposition.DispositionType == "form-data"))
                            {
                                if (String.IsNullOrEmpty(part.Headers.ContentDisposition.FileName))
                                {
                                    string fieldName = Utility.UnquoteToken(part.Headers.ContentDisposition.Name) ?? String.Empty;
                                    string fieldValue = part.ReadAsStringAsync().Result;
                                    paramData.Add(fieldName, fieldValue);
                                }
                            }
                        }
                        else
                        {
                            paramData = actionContext.Request.Content.ReadAsFormDataAsync().Result;
                        }
                    }
                    else
                    {
                        paramData = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);
                    }

                    System.DateTime timestamp = DateTime.MinValue;
                    foreach (var item in paramData.AllKeys.OrderBy(k => k))
                    {
                        string itemValue = HttpUtility.UrlDecode(paramData[item]);
                        sb.Append("&" + item + "=" + itemValue);
                        if (item.ToUpper() == "TIMESTAMP")
                        {
                            timestamp = Convert.ToDateTime(itemValue);
                        }
                    }

                    //Requests are only good for 15 minutes,  so after that time, expire the request
                    TimeSpan ts = System.DateTime.UtcNow - timestamp;
                    if (ts.Minutes > 15)
                    {
                        throw new ApplicationException("The Current Request has expired");
                    }

                    //Check to see if the generated hash matches what was sent to the server
                    string generatedHash = Utility.ComputeHash(secretKey, sb.ToString());
                    if (generatedHash == requestHash)
                    {
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                actionContext.Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new HMACAuthorizationError()
                    {
                        Message = ex.Message
                    }
                );
            }

            actionContext.Response =
            actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                new HMACAuthorizationError()
                { 
                    Message = "Invalid Authorization Credentials"
                }
            );
        }


    }
}