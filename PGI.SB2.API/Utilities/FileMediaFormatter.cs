﻿using PGI.SB2.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PGI.SB2.API.Utilities
{
    public class FileMediaFormatter : MediaTypeFormatter
    {
        public FileMediaFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
        }

        public override bool CanReadType(Type type)
        {
            return type == typeof(FileUpload);
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public async override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            if (!content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var parts = await content.ReadAsMultipartAsync();

            NameValueCollection formData = new NameValueCollection(StringComparer.OrdinalIgnoreCase);
            foreach (var part in parts.Contents.Where(x => x.Headers.ContentDisposition.DispositionType == "form-data"))
            {
                if (String.IsNullOrEmpty(part.Headers.ContentDisposition.FileName))
                {
                    string fieldName = Utility.UnquoteToken(part.Headers.ContentDisposition.Name) ?? String.Empty;
                    string fieldValue = await part.ReadAsStringAsync();
                    formData.Add(fieldName, fieldValue);
                }
            }

            var fileContent = parts.Contents.First(x =>
                SupportedMediaTypes.Contains(x.Headers.ContentType));
            string fileName = fileContent.Headers.ContentDisposition.FileName;
            string mediaType = fileContent.Headers.ContentType.MediaType;

            //var Imgstream = await FileContent.ReadAsStreamAsync();
            return new FileUpload(await fileContent.ReadAsStreamAsync(), mediaType, fileName, formData);
        }

        private byte[] ReadFully(Stream input)
        {
            var Buffer = new byte[16 * 1024];
            using (var Ms = new MemoryStream())
            {
                int Read;
                while ((Read = input.Read(Buffer, 0, Buffer.Length)) > 0)
                {
                    Ms.Write(Buffer, 0, Read);
                }
                return Ms.ToArray();
            }
        }

    }
}