﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;

namespace PGI.SB2.API.Utilities
{
    public class FileUpload
    {
        public Stream FileData { get; set; }
        public string FileName { get; set; }
        public string MediaType { get; set; }
        public NameValueCollection FormData { get; set; }

        public FileUpload(Stream fileData, string mediaType, string fileName, NameValueCollection formData)
        {
            FileData = fileData;
            MediaType = mediaType;
            FileName = fileName.Replace("\"", "");
            FormData = formData;
        }

        /*
        public void Save(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var NewPath = Path.Combine(path, FileName);
            if (File.Exists(NewPath))
            {
                File.Delete(NewPath);
            }

            File.WriteAllBytes(NewPath, Buffer);

            var Property = Value.GetType().GetProperty("FileName");
            Property.SetValue(Value, FileName, null);
        }
        */
    }
}