﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using log4net.Config;
using PGI.SB2.API.Utilities;
using System.Web;
using PGI.SB2.Entity;
using PGI.SB2.API.Models;
using System.Collections.Specialized;
using System.Net.Http.Formatting;
using PGI.SB2.SDK;

namespace PGI.SB2.API.Controllers
{
    public class SessionController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SessionController));

        public SessionController()
        {
            XmlConfigurator.Configure();
        }


        /// <summary>
        /// Read an existing Session from the repository
        /// </summary>
        /// <param name="ID">The ID for the asset to read</param> 
        /// <param name="VersionID">The version of the asset to retrieve, if left blank, the current version will be read</param>
        /// <returns>A response object that contains the Asset information</returns>
        [HttpGet]
        [ActionName("Read")]
        public ReadSession Read(String ID)
        {
            HMACAuthorization authorization = new HMACAuthorization();
            authorization.Authorize(Request, HttpUtility.ParseQueryString(Request.RequestUri.Query), "console");

            ReadSession response = new ReadSession();

            try
            {
                SB2Domain sessionDomain = new SB2Domain(Settings.AWSSessionDomain);
                Session session = new Session();
                if (session.Retrieve(sessionDomain, new System.Guid(ID)) == true)
                {
                    response.ID = session.Id;
                    response.Expiration = session.Expiration;
                    response.Role = session.Role;
                    response.UserName = session.UserName;
                    response.SecretKey = session.SecretKey;
                    return response;
                }
                else
                {
                    throw session.LastError;
                }
            }
            catch (Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception,  the API wil generate the exception message
                log.Error(new
                {
                    ID = ID
                }, ex);
                throw ex;
            }

        }



        /// <summary>
        /// Get Session provides a mechanism for the specified user to log on to the management console, and retreive a
        /// session token.
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns>A response object that contains the ID of the newly created Session</returns>
        [HttpPost]
        [ActionName("Create")]
        public System.Guid Create([FromBody] FormDataCollection FormData)
        {
            
            HMACAuthorization authorization = new HMACAuthorization();
            NameValueCollection parameters = FormData.ReadAsNameValueCollection();
            authorization.Authorize(Request, parameters, "console");

            try
            {
                SB2Domain userDomain = new SB2Domain(Settings.AWSUserDomain);
                User user = new User();
                if (user.Login(userDomain, parameters["UserName"], parameters["Password"]) == true)
                {
                    SB2Domain sessionDomain = new SB2Domain(Settings.AWSSessionDomain);
                    Session session = new Session();
                    if (session.Create(sessionDomain, user.UserName, user.Password, user.Role) == true)
                    {
                        return session.Id;
                    }
                    else
                    {
                        throw new ApplicationException("Could not create session for the specified user");
                    }
                }
                else
                {
                    throw new ApplicationException("Invalid Username or Password");
                }
            }
            catch (Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception,  the API wil generate the exception message
                log.Error(new
                {
                    UserName = parameters["UserName"] ?? "No Username Specified"
                }, ex);
                throw ex;
            }
        }

        /// <summary>
        /// Delete a Session
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns>A response object that contains the ID of the newly created Session</returns>
        [HttpPost]
        [ActionName("Delete")]
        public bool Delete([FromBody] FormDataCollection FormData)
        {
            HMACAuthorization authorization = new HMACAuthorization();
            NameValueCollection parameters = FormData.ReadAsNameValueCollection();
            authorization.Authorize(Request, parameters, "console");

            try
            {
                SB2Domain sessionDomain = new SB2Domain(Settings.AWSSessionDomain);
                Session session = new Session();
                return session.Destory(sessionDomain, new System.Guid(parameters["SessionId"]));
            }
            catch (Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception,  the API wil generate the exception message
                log.Error(new
                {
                    SessionId = parameters["SessionId"] ?? "No Id Specified"
                }, ex);
                throw ex;
            }
        }


    }
}
