﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using log4net;
using log4net.Config;
using StructureMap;
using PGI.SB2.API.Utilities;
using PGI.SB2.Core;
using PGI.SB2.Entity;
using PGI.SB2.SDK;
using PGI.SB2.Plugins;
using PGI.SB2.API.Models;
using System.Collections.Specialized;
using System.Net.Http.Formatting;

namespace PGI.SB2.API.Controllers
{
    public class AssetController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AssetController));

        public AssetController()
        {
            XmlConfigurator.Configure();
        }

        /// <summary>
        /// Search for existing Assets from the repository
        /// </summary>
        /// <param name="Phrase">The criteria to search for</param> 
        /// <returns>A response object that contains the Assets that meet the search criteria</returns>
        [HttpGet]
        [ActionName("Search")]
        public AssetSearchDetail Search(string Phrase, int CurrentPage, int ResultsPerPage)
        {
            HMACAuthorization authorization = new HMACAuthorization();
            authorization.Authorize(Request, HttpUtility.ParseQueryString(Request.RequestUri.Query), "all");

            AssetSearchDetail sdkResults = new AssetSearchDetail();
            try
            {
                SB2Domain assetDomain = new SB2Domain(Settings.AWSAssetDomain);

                PGI.SB2.Entity.Search search = new PGI.SB2.Entity.Search();

                //Translate from the private Entity version of AssetSearchResult to the public SDK version
                AssetSearch assetSearchResponse = search.Assets(assetDomain, Phrase, "", CurrentPage, ResultsPerPage);

                foreach (var entResult in assetSearchResponse.Results)
                {
                    SDK.AssetSearchResult sdkResult = new SDK.AssetSearchResult();
                    sdkResult.AssetID = entResult.AssetID;
                    sdkResult.Name = entResult.Name;
                    sdkResult.CompanyID = entResult.CompanyID;
                    sdkResult.CompanyName = entResult.CompanyName;
                    sdkResult.ModeratorName = entResult.ModeratorName;
                    sdkResult.ModeratorEmail = entResult.ModeratorEmail;
                    sdkResult.RegionName = entResult.RegionName;
                    sdkResult.VersionCount = entResult.VersionCount;
                    sdkResult.ConferenceID = entResult.ConferenceID;
                    sdkResult.ConferenceName = entResult.ConferenceName;
                    sdkResult.SourceFileName = entResult.SourceFileName;
                    sdkResult.SourceFileExtension = entResult.SourceFileExtension;
                    sdkResult.Notes = entResult.Notes;

                    sdkResults.Results.Add(sdkResult);
                }

                sdkResults.PageCount = assetSearchResponse.PageCount;
                sdkResults.ResultCount = assetSearchResponse.ResultCount;
                sdkResults.ResultsPerPage = assetSearchResponse.ResultsPerPage;

                return sdkResults;

            }
            catch (Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception,  the API wil generate the exception message
                log.Error(new
                {
                    Phrase = Phrase
                }, ex);
                throw ex;
            }

        }

        /// <summary>
        /// Read an existing Asset from the repository
        /// </summary>
        /// <param name="ID">The ID for the asset to read</param> 
        /// <param name="VersionID">The version of the asset to retrieve, if left blank, the current version will be read</param>
        /// <returns>A response object that contains the Asset information</returns>
        [HttpGet]
        [ActionName("Read")]
        public ReadAsset Read(string ID, string VersionID = null)
        {
            HMACAuthorization authorization = new HMACAuthorization();
            authorization.Authorize(Request, HttpUtility.ParseQueryString(Request.RequestUri.Query), "all");

            ReadAsset response = new ReadAsset();

            try
            {
                SB2Domain assetDomain = new SB2Domain(Settings.AWSAssetDomain);
                SB2Domain assetVersionDomain = new SB2Domain(Settings.AWSAssetVersionDomain);

                PGI.SB2.Entity.Asset asset = new PGI.SB2.Entity.Asset(new System.Guid(ID));
                if (asset.Retrieve(assetDomain, assetVersionDomain))
                {
                    response = new PGI.SB2.SDK.ReadAsset();
                    response.AssetID = ID;
                    response.Name = asset.Name;
                    response.CompanyID = asset.CompanyID;
                    response.CompanyName = asset.CompanyName;
                    response.ClientID = asset.ClientID;
                    response.ModeratorEmail = asset.ModeratorEmail;
                    response.ModeratorName = asset.ModeratorName;
                    response.ConferenceLink = asset.ConferenceLink;
                    response.ConferenceID = asset.ConferenceID;
                    response.ConferenceName = asset.ConferenceName;
                    response.EventStartUTC = asset.EventStartUTC;
                    response.EventEndUTC = asset.EventEndUTC;
                    response.RegionName = asset.RegionName;
                    response.SourceFileName = asset.SourceFileName;
                    response.SourceFileExtension = asset.SourceFileExtension;
                    response.CurrentVersionID = asset.CurrentVersionID;
                    response.CurrentVersionNumber = asset.VersionCount;
                    response.Versions = new List<AssetVersion>();
                    foreach (var version in asset.Versions)
                    {
                        AssetVersion apiVersion = new AssetVersion()
                        {
                            BucketName = version.BucketName,
                            CreateDateUTC = version.CreateDateUTC,
                            CreateUser = version.CreateUser,
                            Notes = version.Notes,
                            RegionName = version.RegionName,
                            SourceFileName = version.SourceFileName,
                            SourceFileExtension = version.SourceFileExtension,
                            VersionNumber = version.VersionNumber,
                            VersionID = version.VersionID,
                            DownloadURL = asset.DownloadURL(response.VersionID),
                            PluginResults = new List<AssetPluginResult>(),
                            PluginErrorMessage = version.PluginErrorMessage
                        };

                        foreach(var srcResult in version.PluginResults)
                        {
                            apiVersion.PluginResults.Add(new AssetPluginResult()
                            {
                                Id = srcResult.Id,
                                RunAt = srcResult.RunAt,
                                Att = srcResult.Att,
                                Result = srcResult.Result
                            });
                        }

                        response.Versions.Add(apiVersion);

                        if (version.VersionID == VersionID)
                        {
                            response.CurrentVersionID = version.VersionID;
                            response.CurrentVersionNumber = version.VersionNumber;
                        }
                    }

                    response.Versions.Sort(new AssetVersion.AssetVersionComparer());

                    //If the version is not null,  loop through the list and confirm that we have an asset with that 
                    //versionId before we set that to be the current asset.  This way, we don't have to worry about the
                    //asset not existing if a bogus versionId is passed in.

                    response.DownloadURL = asset.DownloadURL(response.CurrentVersionID);
                }
                else
                {
                    throw asset.LastError;
                }
            }
            catch(Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception,  the API wil generate the exception message
                log.Error(new {
                    ID = ID,
                    VersionID = VersionID
                }, ex);
                throw ex;
            }

            return response;
        }

        /// <summary>
        /// Create a new Asset and add it to the Repository.
        /// </summary>
        /// <param name="FileKey">The unique identifier for the file to Add</param> 
        /// <param name="FileStream">The content of the file</param>
        /// <returns>A response object that contains the ID of the newly created Asset</returns>
        [HttpPost]
        [ActionName("Create")]
        public System.Guid Create([FromBody]FileUpload assetUpload)
        {
            System.Guid AssetID = new System.Guid();

            try
            {
                HMACAuthorization authorization = new HMACAuthorization();
                authorization.Authorize(Request, assetUpload.FormData, "all");

                SB2Domain assetDomain = new SB2Domain(Settings.AWSAssetDomain);
                SB2Domain assetVersionDomain = new SB2Domain(Settings.AWSAssetVersionDomain);

                PGI.SB2.Entity.Asset assetEntity = new PGI.SB2.Entity.Asset();

                string regionName = "";
                foreach (var item in assetUpload.FormData.AllKeys)
                {
                    string itemValue = HttpUtility.UrlDecode(assetUpload.FormData[item]);
                    switch (item.ToUpper())
                    {
                        case "NAME": assetEntity.Name = itemValue; break;
                        case "COMPANYID": assetEntity.CompanyID = itemValue; break;
                        case "COMPANYNAME": assetEntity.CompanyName = itemValue; break;
                        case "CLIENTID": assetEntity.ClientID = itemValue; break;
                        case "MODERATORNAME": assetEntity.ModeratorName = itemValue; break;
                        case "MODERATOREMAIL": assetEntity.ModeratorEmail = itemValue; break;
                        case "CONFERENCEID": assetEntity.ConferenceID = itemValue; break;
                        case "CONFERENCENAME": assetEntity.ConferenceName = itemValue; break;
                        case "CONFERENCELINK": assetEntity.ConferenceLink = itemValue; break;
                        case "EVENTSTARTUTC": assetEntity.EventStartUTC = Convert.ToDateTime(itemValue); break;
                        case "EVENTENDUTC": assetEntity.EventEndUTC = Convert.ToDateTime(itemValue); break;
                        case "REGIONNAME": regionName = itemValue; break;
                        case "NOTES": assetEntity.Notes = itemValue; break;
                    }
                }

                if (!string.IsNullOrEmpty(regionName))
                {
                    //TODO: May want to add some business logic about where to store the asset. This could be by business or whatever
                    string bucketName = Settings.GetBucketNameByRegion(regionName);
                    SB2Bucket bucket = new SB2Bucket(bucketName, regionName);
                    assetEntity.SetContent(assetUpload.FileName, assetUpload.FileData);

                    //Save the asset, the authorization.AccessKey is essentially the username of the authenticated
                    //account that made the request.
                    bool bResult = assetEntity.Save(bucket, assetDomain, assetVersionDomain, authorization.AccessKey);

                    if (bResult == true)
                    {
                        Process_Plugins(assetEntity);
                    }
                    else
                    {
                        if (assetEntity.LastError != null)
                        {
                            throw assetEntity.LastError;
                        }
                        else
                        {
                            throw new ApplicationException("The asset could not be saved.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception
                //the API wil generate the exception message

                log.Error(new
                {
                    FileName = assetUpload.FileName,
                    FormData = Utility.ConstructQueryString(assetUpload.FormData)
                }, ex);
                throw ex;
            }

            return AssetID;
        }

        /// <summary>
        ///     Create a new version of an existing asset and put the new version in the repository
        /// </summary>
        /// <param name="assetUpload"></param>
        /// <returns></returns>
        [HttpPut]
        [ActionName("AddVersion")]
        public string AddVersion([FromBody]FileUpload assetUpload)
        {
            string assetID = string.Empty;
            string versionID = string.Empty;
            string notes = string.Empty;

            try
            {
                HMACAuthorization authorization = new HMACAuthorization();
                authorization.Authorize(Request, assetUpload.FormData, "all");

                foreach (var item in assetUpload.FormData.AllKeys)
                {
                    string itemValue = HttpUtility.UrlDecode(assetUpload.FormData[item]);
                    switch (item.ToUpper())
                    {
                        case "ASSETID": assetID = itemValue; break;
                        case "NOTES": notes = itemValue; break;
                    }
                }

                if (!string.IsNullOrEmpty(assetID))
                {
                    SB2Domain assetDomain = new SB2Domain(Settings.AWSAssetDomain);
                    SB2Domain assetVersionDomain = new SB2Domain(Settings.AWSAssetVersionDomain);

                    PGI.SB2.Entity.Asset asset = new PGI.SB2.Entity.Asset(new System.Guid(assetID));
                    if (asset.Retrieve(assetDomain, assetVersionDomain))
                    {
                        //Notes can be set for a new asset or when adding a new version to an existing asset
                        asset.Notes = notes;

                        string bucketName = Settings.GetBucketNameByRegion(asset.RegionName);

                        SB2Bucket bucket = new SB2Bucket(bucketName, asset.RegionName);
                        //Set the new version of the file content
                        asset.SetContent(assetUpload.FileName, assetUpload.FileData);

                        //Save the asset, the authorization.AccessKey is essentially the username of the authenticated
                        //account that made the request.
                        if (asset.Save(bucket, assetDomain, assetVersionDomain, authorization.AccessKey))
                        {
                            Process_Plugins(asset);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception
                //the API wil generate the exception message

                log.Error(new
                {
                    FileName = assetUpload.FileName,
                    FormData = Utility.ConstructQueryString(assetUpload.FormData)
                }, ex);
                throw ex;
            }

            return versionID;
        }

        [HttpPut]
        [ActionName("RunPlugins")]
        public bool RunPlugins([FromBody] FormDataCollection FormData)
        {
            HMACAuthorization authorization = new HMACAuthorization();
            NameValueCollection parameters = FormData.ReadAsNameValueCollection();
            authorization.Authorize(Request, parameters, "all");

            bool bReturn = false;
            string AssetId = parameters["AssetID"];
            string VersionId = parameters["VersionId"];

            try
            {
                SB2Domain assetDomain = new SB2Domain(Settings.AWSAssetDomain);
                SB2Domain assetVersionDomain = new SB2Domain(Settings.AWSAssetVersionDomain);

                PGI.SB2.Entity.Asset asset = new PGI.SB2.Entity.Asset(new System.Guid(AssetId));
                if (asset.Retrieve(assetDomain, assetVersionDomain))
                {
                    bReturn = Process_Plugins(asset, VersionId);
                }
                else
                {
                    throw asset.LastError;
                }
            }
            catch (Exception ex)
            {
                //For API Calls, log the error and than rethrow the exception,  the API wil generate the exception message
                log.Error(new
                {
                    ID = AssetId,
                    VersionID = VersionId
                }, ex);
                throw ex;
            }

            return bReturn;
        }

        /// <summary>
        /// Retireves all Installed Plugins and processes them in identifier order
        /// </summary>
        /// <param name="AssetDetails">The Asset Object to Run through the plugins</param>
        /// <returns>True/False</returns>
        private bool Process_Plugins(Asset AssetDetails, string VersionId = "")
        {
            string ErrorMessage = "";
            List<PluginResult> currentResults = new List<PluginResult>();
            List<PluginResult> previousResults = new List<PluginResult>();

            //Get the Correct version of the asset to process plugins for
            var versionToProcess = AssetDetails.CurrentVersion;
            if (!string.IsNullOrEmpty(VersionId))
            {
                versionToProcess = AssetDetails.Versions.FirstOrDefault(v => v.VersionID == VersionId); 
            }

            if (versionToProcess != null && versionToProcess.PluginResults != null)
            {
                previousResults = versionToProcess.PluginResults;
            }

            //Iterate through the list of plugins
            var pluginList = ObjectFactory.GetAllInstances<IAssetEvent>();
            if (pluginList != null)
            {
                foreach (IAssetEvent plugin in pluginList.OrderBy(p => p.PluginIdentifier))
                {
                    //Check the list of previous results to see if this plugin has been run before
                    PluginResult currentResult = previousResults.FirstOrDefault(r => r.Id == plugin.PluginIdentifier);
                    if (currentResult == null)
                    {
                        currentResult = new PluginResult();
                        currentResult.Id = plugin.PluginIdentifier;
                        currentResult.Result = false;
                    }

                    if (currentResult.Result == false)
                    {
                        currentResult.Att++;
                        currentResult.RunAt = System.DateTime.Now.ToUniversalTime();

                        try
                        {
                            //If this is a new Asset, than the version count will be zero.
                            if (AssetDetails.VersionCount == 0)
                            {
                                currentResult.Result = plugin.AssetCreated(AssetDetails);
                            }
                            else
                            {
                                currentResult.Result = plugin.AssetNewVersion(AssetDetails);
                            }

                            if (currentResult.Result == false)
                            {
                                if (plugin.LastError != null)
                                {
                                    throw plugin.LastError;
                                }
                                else
                                {
                                    throw new ApplicationException("A Plugin Error Occurred");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            currentResult.Result = false;
                            ErrorMessage = ex.Message;
                        }
                    }

                    currentResults.Add(currentResult);
                    if (currentResult.Result == false)
                        break;
                }
            }

            SB2Domain assetVersionDomain = new SB2Domain(Settings.AWSAssetVersionDomain);
                
            AssetDetails.Save_PluginResults(assetVersionDomain, VersionId, currentResults, ErrorMessage);

            if (currentResults.Any(r => r.Result == false))
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}
