﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using log4net.Config;
using PGI.SB2.API.Utilities;
using System.Web;
using PGI.SB2.SDK;
using StructureMap;
using PGI.SB2.Plugins;

namespace PGI.SB2.API.Controllers
{
    public class PluginController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PluginController));

        public PluginController()
        {
            XmlConfigurator.Configure();
        }


        /// <summary>
        /// Retrieve a list of the current plugins.
        /// </summary>
        /// <param name="ID">The ID for the asset to read</param> 
        /// <param name="VersionID">The version of the asset to retrieve, if left blank, the current version will be read</param>
        /// <returns>A response object that contains the Asset information</returns>
        [HttpGet]
        [ActionName("List")]
        public ListPlugins List()
        {
            //HMACAuthorization authorization = new HMACAuthorization();
            //authorization.Authorize(Request, HttpUtility.ParseQueryString(Request.RequestUri.Query), "console");

            ListPlugins response = new ListPlugins();

            try
            {
                foreach (IAssetEvent plugin in ObjectFactory.GetAllInstances<IAssetEvent>())
                {
                    response.Plugins.Add(new PluginDescription()
                    {
                        Name = plugin.PluginName,
                        Identifier = plugin.PluginIdentifier,
                        AssemblyName = plugin.AssemblyName
                    });
                }

                return response;
            }
            catch (Exception ex)
            {
                //For API Calls, log the error and then rethrow the exception,  the API wil generate the exception message
                log.Error("", ex);
                throw ex;
            }

        }
    }
}
