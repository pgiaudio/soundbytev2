﻿using Amazon.SimpleDB.Model;
using log4net;
using log4net.Config;
using PGI.SB2.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;


namespace PGI.SB2.Entity
{
    /// <summary>
    /// An asset is a multimedia file of some type and the metadata associated with that file.  
    /// This class provides the necessary functionality for working with assets
    /// directly within the asset management system.
    /// </summary>
    public class Asset : EntityBase
    {
        public class AssetVersionComparer : IComparer<AssetVersion>
        {
            public int Compare(AssetVersion x, AssetVersion y)
            {
                return x.VersionNumber < y.VersionNumber ? 1 : (x.VersionNumber == y.VersionNumber ? 0 : -1);
            }
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Asset));

        private System.Guid _iD;
        private Stream _content;
        private string _sourceFileName;
        private string _sourceFileExtension;
        private string _currentVersionID;
        private string _regionName;
        private string _bucketName;
        private List<AssetVersion> _versions;
        
        public class AssetVersion
        {
            /// <summary>
            /// The version Identifier for this asset version
            /// </summary>
            public string VersionID { get; set; }

            /// <summary>
            /// The physical location where the asset is stored
            /// </summary>
            public string RegionName { get; set; }

            /// <summary>
            /// The S3 bucket where the asset is stored
            /// </summary>
            public string BucketName { get; set; }

            /// <summary>
            /// The filename of the source file for this version of the asset
            /// </summary>
            public string SourceFileName { get; set; }

            /// <summary>
            /// The file extension for the source file for this version of the asset
            /// </summary>
            public string SourceFileExtension { get; set; }

            /// <summary>
            /// Additional information about this asset version
            /// </summary>
            public string Notes { get; set; }

            /// <summary>
            /// A numerical indicator of the asset version
            /// </summary>
            public int VersionNumber { get; set; }

            /// <summary>
            /// The user that originally created this asset version
            /// </summary>
            public string CreateUser { get; set; }

            /// <summary>
            /// The date and time that the asset version was originally created
            /// </summary>
            public System.DateTime CreateDateUTC { get; set; }

            /// <summary>
            /// The audit trail for the plugins run against this asset version
            /// </summary>
            public List<PluginResult> PluginResults { get; set; }

            /// <summary>
            /// The current Error Message for the plugin
            /// </summary>
            public string PluginErrorMessage { get; set; }
        }

        /// <summary>
        /// The Asset Identifier
        /// </summary>
        public System.Guid ID
        {
            get { return _iD; }
        }

        /// <summary>
        /// The content of the asset
        /// </summary>
        public Stream Content
        {
            get { return _content; }
        }

        /// <summary>
        /// The most recent version identifier for this asset
        /// </summary>
        public string CurrentVersionID
        {
            get { return _currentVersionID; }
        }

        public AssetVersion CurrentVersion
        {
            get
            {
                if (this.Versions != null)
                {
                    return this.Versions.FirstOrDefault(v => v.VersionID == _currentVersionID);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// The physical location where the asset is stored
        /// </summary>
        public string RegionName
        {
            get { return _regionName; }
        }

        /// <summary>
        /// The name of the bucket that this asset belongs to.  
        /// This value is only populated once the asset has been saved, otherwise it will be null
        /// </summary>
        public string BucketName
        {
            get { return _bucketName; }
        }

        /// <summary>
        /// The filename of the source file for this asset
        /// </summary>
        public string SourceFileName
        {
            get { return _sourceFileName; }
        }

        /// <summary>
        /// The file extension for the source file for this asset
        /// </summary>
        public string SourceFileExtension
        {
            get { return _sourceFileExtension; }
        }

        /// <summary>
        /// The number of versions for this asset
        /// </summary>
        public int VersionCount
        {
            get { return _versions.Count; }
        }

        /// <summary>
        /// A collection of different versions of the asset
        /// </summary>
        public List<AssetVersion> Versions
        {
            get { return _versions;  }
        }

        /// <summary>
        /// The name of the asset
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The ID of the company that this asset belongs to
        /// </summary>
        public string CompanyID { get; set; }

        /// <summary>
        /// The Name of the company that this asset belongs to
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// The name of the ClientID for the Moderator that this asset belongs to
        /// </summary>
        public string ClientID { get; set; }

        /// <summary>
        /// The name of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorName { get; set; }

        /// <summary>
        /// The Email Address of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorEmail { get; set; }

        /// <summary>
        /// The Conference ID (if applicable) for this asset
        /// </summary>
        public string ConferenceID { get; set; }

        /// <summary>
        /// The Conference Name (if applicable) for this asset
        /// </summary>
        public string ConferenceName { get; set; }

        /// <summary>
        /// The Conference Link (if applicable) for this asset
        /// </summary>
        public string ConferenceLink { get; set; }

        /// <summary>
        /// The Event Start Time for the conference associated with this asset in UTC
        /// </summary>
        public System.DateTime EventStartUTC { get; set; }

        /// <summary>
        /// The Event End Time for the conference associated with this asset in UTC
        /// </summary>
        public System.DateTime EventEndUTC{ get; set; }

        /// <summary>
        /// The user that originally created this asset
        /// </summary>
        public string CreateUser { get; set; }

        /// <summary>
        /// The date and time that the asset was originally created
        /// </summary>
        public System.DateTime CreateDateUTC { get; set; }

        /// <summary>
        /// The User that most recently modified this asset
        /// </summary>
        public string UpdateUser { get; set; }

        /// <summary>
        /// The Date and time that the asset was last modified
        /// </summary>
        public System.DateTime UpdateDateUTC { get; set; }

        /// <summary>
        /// Additional information about this asset
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Instantiate a new instance of the Asset class.
        /// </summary>
        /// <param name="ID">If specified, this is an existing asset, if left blank, it is a new asset</param> 
        public Asset(System.Guid? ID = null)
        {
            XmlConfigurator.Configure();

            _exists = false;
            _versions = new List<AssetVersion>();

            if (ID == null)
            {
                _iD = Guid.NewGuid();
            }
            else 
            {
                _iD = (System.Guid)ID;
            }
        }

        /// <summary>
        /// Assign the Content Stream for this asset.
        /// </summary>
        /// <param name="FileName">The filename of the asset</param> 
        /// <param name="Content">A stream object containing the content of the specified asset</param>
        /// <returns>void</returns>
        public void SetContent(string FileName, Stream Content)
        {
            _sourceFileName = FileName;
            _sourceFileExtension = Path.GetExtension(_sourceFileName);
            _content = Content;
        }

        /// <summary>
        /// Save a new asset in the specified Bucket/Domains
        /// </summary>
        /// <param name="Bucket">The Bucket Object where the asset will be located</param> 
        /// <param name="AssetDomain">The Asset Domain Object</param>
        /// <param name="AssetVersionDomain">The Asset Version Domain Object</param>
        /// <param name="UserName">The name of the user making this request</param>
        /// <returns>true if the asset was saved successfully/false if the asset was not saved</returns>
        public bool Save(SB2Bucket Bucket, SB2Domain AssetDomain, SB2Domain AssetVersionDomain, string UserName)
        {
            bool bRet = false;
            try
            {
                if (Content != null)
                {
                    _regionName = Bucket.BucketRegion.SystemName;
                    _bucketName = Bucket.BucketName;
                    _currentVersionID = Bucket.AddObject(Get_FileKey(), Content);
                    AssetDomain.UpdateItem(_iD.ToString().ToLower(), Get_AssetAttributes(UserName));
                    AssetVersionDomain.UpdateItem(_currentVersionID, Get_AssetVersionAttributes(UserName));
                    bRet = true;
                }
            }
            catch(Exception ex)
            {
                log.Error("PGI.AVS.Entity.Asset.Save: " + _iD.ToString().ToLower(), ex);
                _lastError = ex;
                _currentVersionID = null;
                bRet = false;
            }

            return bRet;
        }

        /// <summary>
        /// Record the plugin results execution for the current version of the asset
        /// </summary>
        /// <param name="AssetVersionDomain">The Asset Version domain to update</param>
        /// <param name="Results">The list of plugin results</param>
        /// <param name="ErrorMessage">An error message if a failure occurs</param>
        /// <returns></returns>
        public bool Save_PluginResults(SB2Domain AssetVersionDomain, string VersionId, List<PluginResult> Results, string ErrorMessage)
        {
            bool bRet = false;
            try
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();

                List<ReplaceableAttribute> data = new List<ReplaceableAttribute>();
                data.Add(new ReplaceableAttribute() { Name = "PluginResults", Value = ser.Serialize(Results), Replace = true });
                if (ErrorMessage != null)
                {
                    data.Add(new ReplaceableAttribute() { Name = "PluginErrorMessage", Value = ErrorMessage, Replace = true });
                }

                if (string.IsNullOrEmpty(VersionId))
                {
                    VersionId = _currentVersionID;
                }

                AssetVersionDomain.UpdateItem(VersionId, data);
                bRet = true;
            }
            catch (Exception ex)
            {
                log.Error("PGI.AVS.Entity.Asset.Save: " + _iD.ToString().ToLower(), ex);
                _lastError = ex;
                _currentVersionID = null;
                bRet = false;
            }

            return bRet;
        }


        /// <summary>
        /// Retrieve an existing asset from the specified domains
        /// </summary>
        /// <param name="AssetDomain">The Asset Domain Object</param>
        /// <param name="AssetVersionDomain">The Asset Version Domain Object</param>
        /// <returns>true if the asset was found/false if no asset was found</returns>
        public bool Retrieve(SB2Domain AssetDomain, SB2Domain AssetVersionDomain)
        {
            bool bRet = false;
            try
            {
                List<Amazon.SimpleDB.Model.Attribute> data = AssetDomain.RetrieveItem(_iD.ToString());
                if (data != null)
                {
                    Set_AssetAttributes(data);

                    List<Item> versionData = AssetVersionDomain.RetrieveItems("Select * from `" + AssetVersionDomain.DomainName +  "` where AssetID = '" + _iD.ToString().ToLower() + "'");
                    Set_AssetVersionAttributes(versionData);

                    //This asset already exists.
                    _exists = true; 
                    bRet = true;
                }
            }
            catch(Exception ex)
            {
                log.Error("PGI.AVS.Entity.Asset.Retrieve: " + _iD.ToString().ToLower(), ex);
                _lastError = ex;
                bRet = false;
            }

            return bRet;
        }

        /// <summary>
        ///  Generate the FileKey for this Asset
        /// </summary>
        /// <returns>The Generated File Key</returns>
        public string Get_FileKey()
        {
            string fileKey = ID.ToString();
            if (!string.IsNullOrEmpty(_sourceFileExtension))
            {
                fileKey += _sourceFileExtension;
            }

            return fileKey;
        }

        /// <summary>
        /// This is a fully qualified URL to where the Current Version of the Asset can be downloaded.
        /// This method will only work once the asset has been saved.
        /// </summary>
        /// <param name="VersionID">The version of the asset to return</param>
        /// <returns>The URL to download the specified file</returns>
        public string DownloadURL(string VersionID = "")
        {
            if (string.IsNullOrEmpty(VersionID))
            {
                VersionID = _currentVersionID;
            }
            if (!string.IsNullOrEmpty(_bucketName))
            {
                return string.Format("http://{0}.s3-{1}.amazonaws.com/{2}{3}?versionId={4}",
                    _bucketName, _regionName, _iD, _sourceFileExtension, VersionID);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert from the native Amazon attributes into an Asset object
        /// </summary>
        /// <param name="VersionData">An Item list of the attributes to convert</param>
        /// <returns>void</returns>
        private void Set_AssetAttributes(List<Amazon.SimpleDB.Model.Attribute> data)
        {
            foreach (var attribute in data)
            {
                switch (attribute.Name)
                {
                    case "Name": Name = attribute.Value; break;
                    case "CompanyID": CompanyID = attribute.Value; break;
                    case "CompanyName": CompanyName = attribute.Value; break;
                    case "ClientID": ClientID = attribute.Value; break;
                    case "ModeratorName": ModeratorName = attribute.Value; break;
                    case "ModeratorEmail": ModeratorEmail = attribute.Value; break;
                    case "ConferenceID": ConferenceID = attribute.Value; break;
                    case "ConferenceName": ConferenceName = attribute.Value; break;
                    case "ConferenceLink": ConferenceLink = attribute.Value; break;
                    case "EventStartUTC": EventStartUTC = Convert.ToDateTime(attribute.Value); break;
                    case "EventEndUTC": EventEndUTC = Convert.ToDateTime(attribute.Value); break;
                    case "RegionName": _regionName = attribute.Value; break;
                    case "BucketName": _bucketName = attribute.Value; break;
                    case "SourceFileName": _sourceFileName = attribute.Value; break;
                    case "SourceFileExtension": _sourceFileExtension = attribute.Value; break;
                    case "Notes": Notes = attribute.Value; break;
                    case "CurrentVersionID": _currentVersionID = attribute.Value; break;
                    case "CreateUser": CreateUser = attribute.Value; break;
                    case "CreateDateUTC": CreateDateUTC = Convert.ToDateTime(attribute.Value); break;
                    case "UpdateUser": UpdateUser = attribute.Value; break;
                    case "UpdateDateUTC": UpdateDateUTC = Convert.ToDateTime(attribute.Value); break;
                }
            }
        }


        /// <summary>
        /// Convert from the native Amazon attributes into an AssetVersion object
        /// </summary>
        /// <param name="VersionData">An Item list of the attributes to convert</param>
        /// <returns>void</returns>
        private void Set_AssetVersionAttributes(List<Item> VersionData)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            _versions = new List<AssetVersion>();

            if (VersionData != null)
            {
                foreach (var srcVersion in VersionData)
                {
                    AssetVersion assetVersion = new AssetVersion();
                    assetVersion.VersionID = srcVersion.Name;
                    foreach (var attribute in srcVersion.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "RegionName": assetVersion.RegionName = attribute.Value; break;
                            case "BucketName": assetVersion.BucketName = attribute.Value; break;
                            case "SourceFileName": assetVersion.SourceFileName = attribute.Value; break;
                            case "SourceFileExtension": assetVersion.SourceFileExtension = attribute.Value; break;
                            case "Notes": assetVersion.Notes = attribute.Value; break;
                            case "VersionNumber": assetVersion.VersionNumber = Convert.ToInt32(attribute.Value); break;
                            case "CreateUser": assetVersion.CreateUser = attribute.Value; break;
                            case "CreateDateUTC": assetVersion.CreateDateUTC = Convert.ToDateTime(attribute.Value); break;
                            case "PluginErrorMessage": assetVersion.PluginErrorMessage = attribute.Value; break;
                            case "PluginResults":
                                assetVersion.PluginResults = ser.Deserialize<List<PluginResult>>(attribute.Value);
                                break;
                        }
                    }

                    _versions.Add(assetVersion);
                }
            }
        }

        /// <summary>
        /// Convert to the native Amazon attributes from an Asset object
        /// </summary>
        /// <param name="UserName">The name of the user making this request</param>
        /// <returns>A list of attributes in Amazon format</returns>
        private List<ReplaceableAttribute> Get_AssetAttributes(string UserName)
        {
            List<ReplaceableAttribute> data = new List<ReplaceableAttribute>();
            data.Add(new ReplaceableAttribute() { Name = "Name", Value = Name, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "CompanyID", Value = CompanyID, Replace=true });
            data.Add(new ReplaceableAttribute() { Name = "CompanyName", Value = CompanyName, Replace = true });

            data.Add(new ReplaceableAttribute() { Name = "ClientID", Value = ClientID, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "ModeratorName", Value = ModeratorName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "ModeratorEmail", Value = ModeratorEmail, Replace = true });

            data.Add(new ReplaceableAttribute() { Name = "ConferenceID", Value = ConferenceID, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "ConferenceName", Value = ConferenceName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "ConferenceLink", Value = ConferenceLink, Replace = true });

            data.Add(new ReplaceableAttribute() { Name = "EventStartUTC", Value = EventStartUTC.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "EventEndGMT", Value = EventEndUTC.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });

            data.Add(new ReplaceableAttribute() { Name = "BucketName", Value = _bucketName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "RegionName", Value = _regionName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SourceFileName", Value = _sourceFileName.ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SourceFileExtension", Value = _sourceFileExtension.ToLower(), Replace = true });

            data.Add(new ReplaceableAttribute() { Name = "CurrentVersionID", Value = _currentVersionID, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "VersionCount", Value = Convert.ToInt32(_versions.Count + 1).ToString(), Replace = true });

            if (!string.IsNullOrEmpty(Notes))
            {
                data.Add(new ReplaceableAttribute() { Name = "Notes", Value = Notes, Replace = true });
            }

            if (_exists == false)
            {
                data.Add(new ReplaceableAttribute() { Name = "CreateUser", Value = UserName, Replace = true });
                data.Add(new ReplaceableAttribute() { Name = "CreateDateUTC", Value = System.DateTime.Now.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });
            }

            data.Add(new ReplaceableAttribute() { Name = "UpdateUser", Value = UserName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "UpdateDateUTC", Value = System.DateTime.Now.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });

            //This is our temporary search solution  Remove these fields once a better search solution is found
            data.Add(new ReplaceableAttribute() { Name = "SearchName", Value = Name.ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SearchCompanyName", Value = CompanyName.ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SearchModeratorName", Value = ModeratorName.ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SearchModeratorEmail", Value = ModeratorEmail.ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SearchConferenceName", Value = ConferenceName.ToLower(), Replace = true });
            if (!string.IsNullOrEmpty(Notes))
            {
                data.Add(new ReplaceableAttribute() { Name = "SearchNotes", Value = Notes.ToLower(), Replace = true });
            }
            //End of Temporary Search Solution

            return data;
        }

        /// <summary>
        /// Convert to the native Amazon attributes from an AssetVersion object
        /// </summary>
        /// <param name="UserName">The name of the user making this request</param>
        /// <returns>A list of attributes in Amazon format</returns>
        private List<ReplaceableAttribute> Get_AssetVersionAttributes(string UserName)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();

            List<ReplaceableAttribute> data = new List<ReplaceableAttribute>();
            data.Add(new ReplaceableAttribute() { Name = "AssetID", Value = _iD.ToString().ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "BucketName", Value = _bucketName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "RegionName", Value = _regionName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SourceFileName", Value = _sourceFileName.ToLower(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "SourceFileExtension", Value = _sourceFileExtension.ToLower(), Replace = true });
            if (!string.IsNullOrEmpty(Notes))
            {
                data.Add(new ReplaceableAttribute() { Name = "Notes", Value = Notes, Replace = true });
            }
            data.Add(new ReplaceableAttribute() { Name = "VersionNumber", Value = Convert.ToInt32(_versions.Count + 1).ToString("00000"), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "CreateUser", Value = UserName, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "CreateDateUTC", Value = System.DateTime.Now.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });



            //http://pgi-avs-test-asset-bucket.s3-us-west-2.amazonaws.com/d6f94a62-7b61-4771-8451-b7bc74b752d5.jpg
            return data;
        }
    }
}
