﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class PluginResult
    {
        public int Id { get; set; }
        public System.DateTime RunAt { get; set; }
        /// <summary>
        /// Number of attempts to run this plugin
        /// </summary>
        public int Att { get; set; }
        public bool Result { get; set; }
    }
}
