﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class EntityBase
    {
        public Exception _lastError;
        public bool _exists;

        public bool Exists
        {
            get { return _exists; }
        }

        public Exception LastError
        {
            get { return _lastError; }
        }

        public EntityBase()
        {
            _exists = false;
        }
    }
}
