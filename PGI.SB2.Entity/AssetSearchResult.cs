﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class AssetSearch
    {
        public int ResultCount;
        public int ResultsPerPage;
        public int PageCount;
        public List<AssetSearchResult> Results { get; set; }

        public AssetSearch()
        {
            Results = new List<AssetSearchResult>();
        }
    }

    public class AssetSearchResult
    {
        /// <summary>
        /// The ID of the asset
        /// </summary>
        public string AssetID { get; set; }
        /// <summary>
        /// The name of the asset
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The ID of the company that this asset belongs to
        /// </summary>
        public string CompanyID { get; set; }
        /// <summary>
        /// The Name of the company that this asset belongs to
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// The name of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorName { get; set; }
        /// <summary>
        /// The Email Address of the Moderator that this asset belongs to
        /// </summary>
        public string ModeratorEmail { get; set; }
        /// <summary>
        /// The Conference ID (if applicable) for this asset
        /// </summary>
        public string ConferenceID { get; set; }
        /// <summary>
        /// The Conference Name (if applicable) for this asset
        /// </summary>
        public string ConferenceName { get; set; }
        /// <summary>
        /// The Region Name where this asset should be stored
        /// </summary>
        public string RegionName { get; set; }
        /// <summary>
        /// A numerical indicator of the asset version
        /// </summary>
        public int VersionCount { get; set; }
        /// <summary>
        /// The filename of the source file for this asset
        /// </summary>
        public string SourceFileName { get; set; }
        /// <summary>
        /// The file extension for the source file for this asset
        /// </summary>
        public string SourceFileExtension { get; set; }
        /// <summary>
        /// Additional information about this asset
        /// </summary>
        public string Notes { get; set; }
    }
}
