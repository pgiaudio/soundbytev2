﻿using Amazon.S3;
using Amazon.S3.Model;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class SB2Bucket : EntityBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Asset));

        public string BucketName { get; private set; }
        public IAmazonS3 S3Client { get; private set; }
        public Amazon.RegionEndpoint BucketRegion {get; private set;}

        /// <summary>
        /// Instantiate a new Instance of the AVSS3Bucket.
        /// </summary>
        /// <param name="BucketName">The name of the bucket</param> 
        /// <param name="Region">The region identifier</param> 
        public SB2Bucket(string BucketName, string RegionName)
        {
            XmlConfigurator.Configure();

            BucketRegion = PGI.SB2.Entity.Region.GetRegionByName(RegionName);

            S3Client = new AmazonS3Client(BucketRegion);
            this.BucketName = BucketName;
            if (!CheckBucketExists(this.BucketName))
            {
                CreateBucket();
            }
        }

        /// <summary>
        /// Creates a Bucket suitable for containing AVS Assets.
        /// </summary>
        /// <returns>void</returns>
        private void CreateBucket()
        {
            try
            {
                PutBucketRequest putBucketRequest = new PutBucketRequest()
                {
                    BucketName = BucketName,
                    UseClientRegion = true
                };
                PutBucketResponse putBucketResponse = S3Client.PutBucket(putBucketRequest);
                //if (putBucketResponse.)

                //Enable Versioning for this bucket
                PutBucketVersioningRequest putBucketVersioningRequest = new PutBucketVersioningRequest()
                {
                    BucketName = BucketName,
                    VersioningConfig = new S3BucketVersioningConfig()
                    {
                        Status = VersionStatus.Enabled
                    }
                };
                S3Client.PutBucketVersioning(putBucketVersioningRequest);
            }
            catch(Exception ex)
            {
                _lastError = ex;
                log.Error("PGI.AVS.Entity.AVSS3Bucket.CreateBucket", ex);
            }
        }

        /// <summary>
        /// Validates whether a bucket with a given name is already present.
        /// </summary>
        /// <param name="BucketName">The name of the bucket</param> 
        /// <returns>true if the bucket exists/false if it does not</returns>
        public bool CheckBucketExists(string BucketName)
        {
            bool bRet = false;
            ListBucketsResponse listBucketsResponse = S3Client.ListBuckets();

            Amazon.S3.Model.S3Bucket bucket = listBucketsResponse.Buckets.FirstOrDefault(b => b.BucketName == BucketName);
            if (bucket != null)
            {
                bRet = true;
            }
            return bRet;
        }

        /// <summary>
        /// Deletes the bucket with the specified bucketName.
        /// </summary>
        /// <param name="BucketName">The name of the bucket</param> 
        /// <returns>Void</returns>
        public void DeleteBucket()
        {
            try
            {
                if (CheckBucketExists(BucketName))
                {
                    DeleteBucketRequest deleteBucketRequest = new DeleteBucketRequest()
                    {
                        BucketName = BucketName,
                        UseClientRegion = true
                    };
                    S3Client.DeleteBucket(deleteBucketRequest);
                }
            }

            catch (Exception ex)
            {
                _lastError = ex;
                log.Error("PGI.AVS.Entity.AVSS3Bucket.DeleteBucket", ex);
            }
        }

        /// <summary>
        /// Add a new Instance of an Object with the specified FileKey to the Bucket.
        /// </summary>
        /// <param name="FileKey">The unique identifier for the file to Add</param> 
        /// <param name="FileStream">The content of the file</param>
        /// <returns>the versionId of the added object or null if the add was not successful</returns>
        public string AddObject(string FileKey, Stream FileStream)
        {
            string versionId = null;

            try
            {
                if (CheckBucketExists(BucketName))
                {
                    PutObjectRequest putObjectRequest = new PutObjectRequest();
                    putObjectRequest.BucketName = BucketName;
                    putObjectRequest.CannedACL = S3CannedACL.PublicRead;
                    putObjectRequest.Key = FileKey;
                    putObjectRequest.InputStream = FileStream;
                    PutObjectResponse putObjectResponse = S3Client.PutObject(putObjectRequest);
                    versionId = putObjectResponse.VersionId;
                }
                else
                {

                    throw new ApplicationException(string.Format("Bucket {0} does not exist", BucketName));
                }
            }
            catch(Exception ex)
            {
                _lastError = ex;
                log.Error("PGI.AVS.Entity.AVSS3Bucket.AddObject: " + FileKey, ex);
                throw;
            }

            return versionId;
        }


    }
}
