﻿using Amazon.SimpleDB.Model;
using log4net;
using log4net.Config;
using PGI.SB2.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class Session : EntityBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Search));

        /// <summary>
        /// The ID for the Session
        /// </summary>
        public System.Guid Id { get; private set; }
        /// <summary>
        /// The UserName for the Session
        /// </summary>
        public string UserName { get; private set; }
        /// <summary>
        /// The SecretKey for the Session
        /// </summary>
        public string SecretKey { get; private set; }
        /// <summary>
        /// The Account Role for the Session
        /// </summary>
        public string Role { get; private set; }
        /// <summary>
        /// The Expiration Date of this Session
        /// </summary>
        public System.DateTime Expiration { get; private set; }

        public Session()
        {
            XmlConfigurator.Configure();
        }

        public bool Retrieve(SB2Domain SessionDomain, System.Guid Id)
        {
            try
            {
                List<Amazon.SimpleDB.Model.Attribute> data = SessionDomain.RetrieveItem(Id.ToString());
                if (data != null)
                {
                    this.Id = Id;
                    Set_SessionAttributes(data);
                    return true;
                }

                throw new ApplicationException("Could not retrieve the specified session");
            }
            catch (Exception ex)
            {
                log.Error("Session.Retrieve: " + Id.ToString().ToLower(), ex);
                _lastError = ex;
            }

            return false;
        }



        /// <summary>
        /// Create a New Session
        /// </summary>
        /// <param name="Client">The Amazon DynamoDB Object Wrapper</param>
        /// <param name="Email">The password for the new account</param>
        /// <returns>true if success/false if an error occurs</returns>
        public bool Create(SB2Domain SessionDomain, string UserName, string SecretKey, string Role)
        {
            try
            {
                Id = System.Guid.NewGuid();
                this.UserName = UserName;
                this.Role = Role;
                this.Expiration = System.DateTime.Now.AddDays(14);

                List<ReplaceableAttribute> data = new List<ReplaceableAttribute>();

                data.Add(new ReplaceableAttribute() { Name = "UserName", Value = UserName, Replace = true });
                data.Add(new ReplaceableAttribute() { Name = "SecretKey", Value = SecretKey, Replace = true });
                data.Add(new ReplaceableAttribute() { Name = "Role", Value = Role, Replace = true });
                data.Add(new ReplaceableAttribute() { Name = "Expiration", Value = Expiration.ToString(Constants.UTC_FORMAT), Replace = true });

                SessionDomain.UpdateItem(Id.ToString(), data);

                return true;
            }
            catch (Exception ex)
            {
                log.Error("Session.Create: " + UserName.ToString().ToLower(), ex);
                _lastError = ex;
            }

            return false;
        }


        /// <summary>
        /// Destory an existing Session
        /// </summary>
        /// <param name="Client">The Amazon DynamoDB Object Wrapper</param>
        /// <param name="Email">The password for the new account</param>
        /// <returns>true if success/false if an error occurs</returns>
        public bool Destory(SB2Domain SessionDomain, System.Guid SessionID)
        {
            try
            {
                return SessionDomain.DeleteItem(SessionID.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Session.Destory: " + SessionID.ToString(), ex);
                _lastError = ex;
            }

            return false;
        }


        private void Set_SessionAttributes(List<Amazon.SimpleDB.Model.Attribute> Data)
        {
            foreach (var attribute in Data)
            {
                switch (attribute.Name)
                {
                    case "UserName": UserName = attribute.Value; break;
                    case "SecretKey": SecretKey = attribute.Value; break;
                    case "Role": Role = attribute.Value; break;
                    case "Expiration": Expiration = Convert.ToDateTime(attribute.Value); break;
                }
            }
        }
    }
}
