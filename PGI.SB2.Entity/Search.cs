﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.CloudSearch;
using Amazon.CloudSearch.Model;

namespace PGI.SB2.Entity
{
    public class Search : EntityBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Search));

        public  Search()
        {
            XmlConfigurator.Configure();
        }

        public AssetSearch Assets(SB2Domain AssetDomain, string Phrase, string OrderBy, int CurrentPage, int ResultsPerPage)
        {
            AssetSearch searchResults = new AssetSearch();

            if (!string.IsNullOrEmpty(Phrase))
            {
                //Eventually, I would like to use CloudSearch to replace the sorting capability here.  CloudSearch supports
                //Full text indexes and would be a much faster and more powerful solution.
                string whereClause = string.Format(
                    @"where ItemName() = '{0}' or SearchName like '%{0}%' or CompanyID = '{0}' or SearchCompanyName like '%{0}%' or
                      ConferenceID = '{0}' or SearchConferenceName like '%{0}%' or ClientID = '{0}' or SearchModeratorName like '%{0}%' or SearchModeratorEmail = '{0}' or
                      SourceFileName = '{0}' or SourceFileExtension = '{0}' or SearchNotes like '%{0}%'", Phrase.ToLower());

                PagedQueryItemResult queryResults = AssetDomain.PagedQueryItems("*", AssetDomain.DomainName, whereClause, "", CurrentPage, ResultsPerPage, true);

                searchResults.PageCount = queryResults.PageCount;
                searchResults.ResultCount = queryResults.ResultCount;
                searchResults.ResultsPerPage = queryResults.ResultsPerPage;

                if (queryResults.Items != null)
                {

                    foreach (var result in queryResults.Items)
                    {
                        AssetSearchResult asset = new AssetSearchResult();
                        asset.AssetID = result.Name;

                        foreach (var attribute in result.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "Name": asset.Name = attribute.Value; break;
                                case "CompanyID": asset.CompanyID = attribute.Value; break;
                                case "CompanyName": asset.CompanyName = attribute.Value; break;
                                case "ModeratorName": asset.ModeratorName = attribute.Value; break;
                                case "ModeratorEmail": asset.ModeratorEmail = attribute.Value; break;
                                case "RegionName": asset.RegionName = attribute.Value; break;
                                case "VersionCount": asset.VersionCount = Convert.ToInt32(attribute.Value); break;
                                case "ConferenceID": asset.ConferenceID = attribute.Value; break;
                                case "ConferenceName": asset.ConferenceName = attribute.Value; break;
                                case "SourceFileName": asset.SourceFileName = attribute.Value; break;
                                case "SourceFileExtension": asset.SourceFileExtension = attribute.Value; break;
                                case "Notes": asset.Notes = attribute.Value; break;
                            }
                        }

                        searchResults.Results.Add(asset);
                    }
                } 
            }

            return searchResults;
        }
        //This code was for trying out the Amazon Cloud Search features,  for this implementation, we are using the SimpleDB query capabilities instead.
        /*
        private IAmazonCloudSearch _csClient;
        private string _domainName;

        public string DomainName
        {
            get { return _domainName; }
        }

        public IAmazonCloudSearch CSClient
        {
            get { return _csClient; }
        }

        /// <summary>
        /// Instantiate a new Instance of the AVSSearch.
        /// </summary>
        /// <param name="DomainName">The name of the domain</param> 
        /// <param name="Region">The region identifier</param> 
        public Search(string DomainName, Amazon.RegionEndpoint Region)
        {
            XmlConfigurator.Configure();

            _csClient = new AmazonCloudSearchClient(Region);
            _domainName = DomainName;
            if (!CheckDomainExists(_domainName))
            {
                CreateDomain();
            }
        }

        /// <summary>
        /// Creates a new Search Domain
        /// </summary>
        /// <returns>void</returns>
        private void CreateDomain()
        {
            try
            {
                CreateDomainRequest createDomainRequest = new CreateDomainRequest()
                {
                    DomainName = _domainName
                };
                CreateDomainResponse createDomainResponse = _csClient.CreateDomain(createDomainRequest);
            }
            catch (Exception ex)
            {
                _lastError = ex;
                log.Error("PGI.AVS.Entity.AVSSearch.CreateDomain: " + _domainName, ex);
            }
        }

        /// <summary>
        /// Validates whether a domain with a given name is already present.
        /// </summary>
        /// <param name="DomainName">The name of the domain to check</param> 
        /// <returns>true if the domain exists / false if it does not</returns>
        public bool CheckDomainExists(string DomainName)
        {
            bool bRet = false;
            DescribeDomainsResponse describeDomainsResponse = _csClient.DescribeDomains();

            DomainStatus domain = describeDomainsResponse.DomainStatusList.FirstOrDefault(d => d.DomainName == DomainName);
            if (domain != null)
            {
                bRet = true;
            }

            return bRet;
        }
        */
    }
}
