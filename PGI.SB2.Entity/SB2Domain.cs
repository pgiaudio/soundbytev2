﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amazon.SimpleDB;
using Amazon.SimpleDB.Model;

namespace PGI.SB2.Entity
{
    public class SB2Domain : EntityBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SB2Domain));

        public string DomainName { get; private set; }
        public IAmazonSimpleDB SDBClient {get; private set;}

        /// <summary>
        /// Instantiate a new Instance of the AVSSDBDomain.
        /// </summary>
        /// <param name="DomainName">The name of the domain</param> 
        /// <param name="Region">The region identifier</param> 
        public SB2Domain(string DomainName)
        {
            XmlConfigurator.Configure();

            SDBClient = new AmazonSimpleDBClient(Settings.AWSDefaultRegion);
            this.DomainName = DomainName;
            if (!CheckDomainExists(DomainName))
            {
                CreateDomain();
            }
        }


        /// <summary>
        /// Creates a new Domain
        /// </summary>
        /// <returns>void</returns>
        private void CreateDomain()
        {
            try
            {
                CreateDomainRequest createDomainRequest = new CreateDomainRequest()
                {
                    DomainName = DomainName
                };
                CreateDomainResponse createDomainResponse = SDBClient.CreateDomain(createDomainRequest);
            }
            catch(Exception ex)
            {
                _lastError = ex;
                log.Error(new { Domain = DomainName }, ex);
            }
        }

        /// <summary>
        /// Validates whether a domain with a given name is already present.
        /// </summary>
        /// <param name="DomainName">The name of the domain to check</param> 
        /// <returns>true if the domain exists / false if it does not</returns>
        public bool CheckDomainExists(string DomainName)
        {
            bool bRet = false;
            ListDomainsRequest listDomainsRequest = new ListDomainsRequest();
            ListDomainsResponse response = SDBClient.ListDomains(listDomainsRequest);
            if (response != null)
            {
                bRet = response.DomainNames.Contains(DomainName);
            }

            return bRet;
        }

        /// <summary>
        /// Update the Data Attributes for the specifeid ItemName
        /// </summary>
        /// <param name="ItemName">The key for the item to update</param> 
        /// <param name="Attributes">A list of the attributes to add/update for the specified key</param> 
        /// <returns>void</returns>
        public void UpdateItem(string ItemName, List<ReplaceableAttribute> Attributes)
        {
            try
            {
                PutAttributesRequest request = new PutAttributesRequest()
                {
                    DomainName = DomainName,
                    ItemName = ItemName
                };

                request.Attributes = Attributes;
                SDBClient.PutAttributes(request);
            }
            catch (Exception ex)
            {
                _lastError = ex;
                log.Error(new { ItemName = ItemName }, ex);
                throw;
            }
        }

        /// <summary>
        /// Retrieve All Attributes for the specifeid ItemName
        /// </summary>
        /// <param name="ItemName">The key for the item to retrieve</param> 
        /// <returns>List of Attributes</returns>
        public List<Amazon.SimpleDB.Model.Attribute> RetrieveItem(string ItemName)
        {
            GetAttributesRequest request = new GetAttributesRequest()
            {
                DomainName = DomainName,
                ItemName = ItemName
            };

            try
            {
                GetAttributesResponse response = SDBClient.GetAttributes(request);
                if (response.Attributes.Count > 0)
                {
                    return response.Attributes;
                }
            }
            catch(Exception ex)
            {
                _lastError = ex;
                log.Error(new { ItemName = ItemName }, ex);
            }

            return null;
        }

        /// <summary>
        /// Execute a SQL Command to retrieve a list of Items
        /// </summary>
        /// <param name="SelectExpression">The SQL to execute</param> 
        /// <returns>List of Items</returns>
        public List<Item> RetrieveItems(string SelectExpression)
        {
            try
            {
                SelectRequest request = new SelectRequest();
                request.SelectExpression = SelectExpression;
                request.ConsistentRead = true;
                SelectResponse response = SDBClient.Select(request);
                if (response.Items.Count > 0)
                {
                    return response.Items;
                }
            }
            catch(Exception ex)
            {
                _lastError = ex;
                log.Error(new { Expression = SelectExpression }, ex);
            }

            return null;
        }

        /// <summary>
        /// Delete the specified Item
        /// </summary>
        /// <param name="ItemName">The key for the item to retrieve</param> 
        /// <returns>List of Attributes</returns>
        public bool DeleteItem(string ItemName)
        {
            DeleteAttributesRequest request = new DeleteAttributesRequest()
            {
                DomainName = DomainName,
                ItemName = ItemName
            };

            try
            {
                DeleteAttributesResponse response = SDBClient.DeleteAttributes(request);
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _lastError = ex;
                log.Error(new { ItemName = ItemName }, ex);
            }

            return false;
        }


        /// <summary>
        /// Execute a SQL Command to retrieve a list of Items
        /// </summary>
        /// <param name="SelectExpression">The SQL to execute</param> 
        /// <returns>List of Items</returns>
        public PagedQueryItemResult PagedQueryItems(string ColumnList, string TableName,
            string WhereClause, string OrderByClause, int CurrentPage, int ResultsPerPage, bool ConsistentRead = true)
        {
            try
            {
                PagedQueryItemResult searchResult = new PagedQueryItemResult();

                //First Query is used to find the total number of results
                SelectRequest countRequest = new SelectRequest();
                countRequest.SelectExpression = "SELECT count(*) FROM `" + TableName + "` " + WhereClause;
                countRequest.ConsistentRead = ConsistentRead;
                SelectResponse countResponse = SDBClient.Select(countRequest);

                searchResult.ResultCount = Convert.ToInt32(countResponse.Items.First().Attributes.First(a => a.Name == "Count").Value);
                searchResult.ResultsPerPage = ResultsPerPage;
                searchResult.PageCount = searchResult.ResultCount / ResultsPerPage;
                if (searchResult.ResultCount % ResultsPerPage > 0)
                {
                    searchResult.PageCount++;
                }

                SelectRequest queryRequest = new SelectRequest();
                //If the current Page is greater than the first, than run a query to move the cursor to the first result using the NextToken.
                if (CurrentPage > 0)
                {
                    int skipCount = CurrentPage * ResultsPerPage;
                    SelectRequest pageRequest = new SelectRequest();
                    pageRequest.SelectExpression = "SELECT count(*) FROM `" + TableName + "` " + WhereClause + " " + OrderByClause + " LIMIT " + skipCount.ToString();
                    pageRequest.ConsistentRead = ConsistentRead;
                    SelectResponse pageResponse = SDBClient.Select(pageRequest);
                    queryRequest.NextToken = pageResponse.NextToken;
                }

                queryRequest.SelectExpression = "SELECT " + ColumnList + " FROM `" + TableName + "` " + WhereClause + " " + OrderByClause + " LIMIT " + ResultsPerPage.ToString();
                queryRequest.ConsistentRead = ConsistentRead;
                SelectResponse queryResponse = SDBClient.Select(queryRequest);

                searchResult.Items = queryResponse.Items;

                return searchResult;
            }
            catch (Exception ex)
            {
                _lastError = ex;
                log.Error(new
                {
                    ColumnList = ColumnList,
                    TableName = TableName,
                    WhereClause = WhereClause,
                    OrderByClause = OrderByClause
                }, ex);
            }

            return null;
        }
    }
}
