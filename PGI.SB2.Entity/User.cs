﻿using Amazon.SimpleDB.Model;
using log4net;
using log4net.Config;
using PGI.SB2.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class User : EntityBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(User));
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        public string UserName { get; set; }
        public string Email { get; set; }
        public System.DateTime CreateDateGMT { get; set; }
        public System.DateTime UpdateDateGMT { get; set; }
        public bool Active { get; set; }
        public string Role { get; set; }

        public string Password { get; set; }
        public string PasswordSalt { get; private set; }

        public User()
        {
            XmlConfigurator.Configure();
            Active = true;
        }

        /// <summary>
        /// Retrieve an existing user from the data store
        /// </summary>
        /// <param name="UserDomain">The Amazon Domain Object Wrapper</param>
        /// <param name="UserName">The UserName for the user to retrieve</param>
        /// <returns>true if the user was found/false if no user was found</returns>
        public bool Retrieve(SB2Domain UserDomain, string UserName)
        {
            bool bRet = false;
            try
            {
                List<Amazon.SimpleDB.Model.Attribute> data = UserDomain.RetrieveItem(UserName);
                if (data != null)
                {
                    this.UserName = UserName; 
                    Set_UserAttributes(data);

                    //This user already exists.
                    _exists = true;
                    bRet = true;
                }
            }
            catch(Exception ex)
            {
                log.Error("PGI.AVS.Entity.User.Retrieve: " + UserName.ToString().ToLower(), ex);
                _lastError = ex;
                bRet = false;
            }

            return bRet;
        }

        /// <summary>
        /// Logs an existing user into the system, if successful, this will also populate the user object with the specified user details
        /// </summary>
        /// <param name="UserDomain">The Amazon Domain Object Wrapper</param>
        /// <param name="UserName">The UserName for the user</param>
        /// <param name="Password">The entered Password for the user</param>
        /// <returns>true if the password is correct/false if the password is incorrect</returns>
        public bool Login(SB2Domain UserDomain, string UserName, string UserPassword)
        {
            bool bRet = false;
            try
            {
                List<Amazon.SimpleDB.Model.Attribute> data = UserDomain.RetrieveItem(UserName);
                if (data != null)
                {
                    foreach (var attribute in data)
                    {
                        switch (attribute.Name)
                        {
                            case "Password": Password = attribute.Value; break;
                            case "PasswordSalt": PasswordSalt = attribute.Value; break;
                        }
                    }

                    if (!string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(PasswordSalt))
                    {
                        string hashToValidate = CreatePasswordHash(UserPassword, PasswordSalt);
                        if (hashToValidate == Password)
                        {
                            //Set the user Attributes for this user
                            this.UserName = UserName;
                            Set_UserAttributes(data);
                            bRet = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("PGI.AVS.Entity.User.Login: " + UserName.ToString().ToLower(), ex);
                _lastError = ex;
                bRet = false;
            }

            return bRet;
        }

        /// <summary>
        /// Adds a new User to the System or Updates an existing user
        /// </summary>
        /// <param name="UserDomain">The Amazon Domain Object Wrapper</param>
        /// <returns>true if success/false if an error occurs</returns>
        public bool Save(SB2Domain UserDomain)
        {
            bool bRet = false;
            try
            {
                List<ReplaceableAttribute> data = Get_UserAttributes();

                if (_exists == false)
                {
                    data = Get_UserAttributes();
                    //Check to see if the username/email is already taken
                    List<Item> items = UserDomain.RetrieveItems(string.Format("Select * from `{0}` where ItemName() = '{1}' or Email = '{2}'", UserDomain.DomainName, UserName, Email));
                    if (items != null && items.Count > 0)
                    {
                        throw new ApplicationException("Cannot create new user, A user with this UserName or Email already exists, to update that user, use the Retrieve method first.");
                    }

                    //If this is a new user, than we need to set the Password values to how they should be stored in the database.
                    
                    PasswordSalt = CreatePasswordSalt();
                    string encPassword = CreatePasswordHash(Password, PasswordSalt);

                    data.Add(new ReplaceableAttribute() { Name = "PasswordSalt", Value = PasswordSalt, Replace = true });
                    data.Add(new ReplaceableAttribute() { Name = "Password", Value = encPassword, Replace = true });
                }

                UserDomain.UpdateItem(UserName.ToLower(), data);
                _exists = true;
                bRet = true;
            }
            catch (Exception ex)
            {
                log.Error("PGI.AVS.Entity.User.Save: " + UserName.ToString().ToLower(), ex);
                _lastError = ex;
                bRet = false;
            }

            return bRet;
        }

        /// <summary>
        ///  Convert from the native Amazon attributes into a user object
        /// </summary>
        /// <param name="Data">A list of the attributes from Amazon</param>
        /// <returns>void</returns>
        private void Set_UserAttributes(List<Amazon.SimpleDB.Model.Attribute> Data)
        {
            foreach (var attribute in Data)
            {
                switch (attribute.Name)
                {
                    case "Email": Email = attribute.Value; break;
                    case "Active": Active = Convert.ToBoolean(attribute.Value); break;
                    case "CreateDateGMT": CreateDateGMT = Convert.ToDateTime(attribute.Value); break;
                    case "UpdateDateGMT": UpdateDateGMT = Convert.ToDateTime(attribute.Value); break;
                    case "Password": Password = attribute.Value; break;
                    case "Role": Role = attribute.Value; break;
                }
            }
        }

        /// <summary>
        /// Convert to the native Amazon attributes from a user object
        /// </summary>
        /// <returns>A list of attributes in Amazon format</returns>
        private List<ReplaceableAttribute> Get_UserAttributes()
        {
            List<ReplaceableAttribute> data = new List<ReplaceableAttribute>();
            data.Add(new ReplaceableAttribute() { Name = "Email", Value = Email, Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "Active", Value = Active.ToString(), Replace = true });
            data.Add(new ReplaceableAttribute() { Name = "Role", Value = Role.ToString(), Replace = true });

            if (_exists == false)
            {
                data.Add(new ReplaceableAttribute() { Name = "CreateDateGMT", Value = System.DateTime.Now.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });
            }
            data.Add(new ReplaceableAttribute() { Name = "UpdateDateGMT", Value = System.DateTime.Now.ToUniversalTime().ToString(Constants.UTC_FORMAT), Replace = true });

            return data;
        }

        /// <summary>
        /// Generate a cryptographic random number using the cryptographic service provider
        /// </summary>
        /// <returns>the passwordSalt</returns>
        private string CreatePasswordSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[12];
            rng.GetBytes(buff);
            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }

        /// <summary>
        /// Generate a Hashed password from a plain text password and salt
        /// </summary>
        /// <param name="Pwd">Plaintext Password</param>
        /// <param name="PwdSalt">The Salt to add to the password</param>
        /// <returns>the hashed password</returns>
        private string CreatePasswordHash(string Pwd, string PwdSalt)
        {
            // Create a new instance of the hash crypto service provider.
            HashAlgorithm hashAlg = new SHA256CryptoServiceProvider(); 
            // Convert the data to hash to an array of Bytes.
            byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(Pwd + PwdSalt);
            // Compute the Hash. This returns an array of Bytes.
            byte[] bytHash = hashAlg.ComputeHash(bytValue);

            return Convert.ToBase64String(bytHash);
        }

    }
}
