﻿using PGI.SB2.Entity;
using System;

namespace PGI.SB2.Entity
{
    public sealed partial class Settings
    {

        public Settings() {

        }

        public static string AWSAccessKey
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAccessKey"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAccessKey"];
                else
                    return "";
            }
        }

        public static string AWSSecretAccessKey
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSSecretKey"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSSecretKey"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves a RegionEndpoint object for the Default/US Region
        /// </summary>
        /// <returns>the Amazon.RegionEndpoint</returns>
        public static Amazon.RegionEndpoint AWSDefaultRegion
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSDefaultRegion"]))
                    return Region.GetRegionByName(System.Configuration.ConfigurationManager.AppSettings["AWSDefaultRegion"]);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves a RegionEndpoint object for the European Region
        /// </summary>
        /// <returns>the Amazon.RegionEndpoint</returns>
        public static Amazon.RegionEndpoint AWSEURegion
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSEURegion"]))
                    return Region.GetRegionByName(System.Configuration.ConfigurationManager.AppSettings["AWSEURegion"]);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves a RegionEndpoint object for the Asia Pacific Region
        /// </summary>
        /// <returns>the Amazon.RegionEndpoint</returns>
        public static Amazon.RegionEndpoint AWSAPRegion
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAPRegion"]))
                    return Region.GetRegionByName(System.Configuration.ConfigurationManager.AppSettings["AWSAPRegion"]);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves a RegionEndpoint object for the South America Region
        /// </summary>
        /// <returns>the Amazon.RegionEndpoint</returns>
        public static Amazon.RegionEndpoint AWSSARegion
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSSARegion"]))
                    return Region.GetRegionByName(System.Configuration.ConfigurationManager.AppSettings["AWSSARegion"]);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the name for the User Domain
        /// </summary>
        /// <returns>string</returns>
        public static string AWSUserDomain
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSUserDomain"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSUserDomain"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the name for the Asset Domain
        /// </summary>
        /// <returns>string</returns>
        public static string AWSAssetDomain
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAssetDomain"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAssetDomain"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the name for the AssetVersion Domain
        /// </summary>
        /// <returns>string</returns>
        public static string AWSAssetVersionDomain
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAssetVersionDomain"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAssetVersionDomain"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the name for the Session Domain
        /// </summary>
        /// <returns>string</returns>
        public static string AWSSessionDomain
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSSessionDomain"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSSessionDomain"];
                else
                    return "";
            }
        }


        /// <summary>
        /// Retrieves the name for the US Asset Bucket
        /// </summary>
        /// <returns>string</returns>
        public static string AWSAssetBucketUS
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketUS"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketUS"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the name for the Europe Asset Bucket
        /// </summary>
        /// <returns>string</returns>
        public static string AWSAssetBucketEU
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketEU"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketEU"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the name for the Asia Pacific Asset Bucket
        /// </summary>
        /// <returns>string</returns>
        public static string AWSAssetBucketAP
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketAP"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketAP"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the name for the South America Asset Bucket
        /// </summary>
        /// <returns>string</returns>
        public static string AWSAssetBucketSA
        {
            get
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketSA"]))
                    return System.Configuration.ConfigurationManager.AppSettings["AWSAssetBucketSA"];
                else
                    return "";
            }
        }

        /// <summary>
        /// Retrieves the correct bucket key for S3 for a given region
        /// </summary>
        /// <param name="RegionName">The name of the region</param>
        /// <returns>string</returns>
        public static String GetBucketNameByRegion(string RegionName)
        {
            switch (RegionName.ToLower())
            {
                case "us-east-1":
                    return AWSAssetBucketUS;
                case "us-west-1":
                    return AWSAssetBucketUS;
                case "us-west-2":
                    return AWSAssetBucketUS;
                case "eu-west-1":
                    return AWSAssetBucketEU;
                case "ap-northeast-1":
                    return AWSAssetBucketAP;
                case "ap-southeast-1":
                    return AWSAssetBucketAP;
                case "ap-southeast-2":
                    return AWSAssetBucketAP;
                case "sa-east-1":
                    return AWSAssetBucketSA;
            }

            return "";
        }


    }
}