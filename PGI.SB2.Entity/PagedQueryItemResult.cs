﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public class PagedQueryItemResult
    {
        public int ResultCount { get; set; }
        public int ResultsPerPage { get; set; }
        public int PageCount { get; set; }
        public List<Amazon.SimpleDB.Model.Item> Items { get; set; }
    }
}
