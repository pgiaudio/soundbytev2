﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.SB2.Entity
{
    public static class Region
    {
        /// <summary>
        /// Retrieves a RegionEndpoint for a given region
        /// </summary>
        /// <param name="RegionName">The name of the region</param>
        /// <returns>Amazon.RegionEndpoint</returns>
        public static Amazon.RegionEndpoint GetRegionByName(string RegionName)
        {
            switch (RegionName.ToLower())
            {
                case "us-east-1":
                    return Amazon.RegionEndpoint.USEast1;
                case "us-west-1":
                    return Amazon.RegionEndpoint.USWest1;
                case "us-west-2":
                    return Amazon.RegionEndpoint.USWest2;
                case "eu-west-1":
                    return Amazon.RegionEndpoint.EUWest1;
                case "ap-northeast-1":
                    return Amazon.RegionEndpoint.APNortheast1;
                case "ap-southeast-1":
                    return Amazon.RegionEndpoint.APSoutheast1;
                case "ap-southeast-2":
                    return Amazon.RegionEndpoint.APSoutheast2;
                case "sa-east-1":
                    return Amazon.RegionEndpoint.SAEast1;
            }

            return null;
        }
    }
}
