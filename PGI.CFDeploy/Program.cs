﻿using Amazon.CloudFormation;
using Amazon.CloudFormation.Model;
using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGI.CFDeploy
{
    // Define a class to receive parsed values
    public class Options
    {
        [Option('n', "name", Required = true, HelpText = "The Name of the Stack to Create. Must contain only alphanumeric characters (case sensitive) and start with an alpha character. Maximum length of the name is 255 characters.")]
        public string StackName { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        enum ExitCode : int
        {
            Success = 0,
            Error = 1
        }

        static void Main(string[] args)
        {
            try
            {
                foreach (var arg in args)
                {
                    Console.WriteLine("Arguments: " + arg);
                }

                var options = new Options();
                CommandLine.Parser.Default.ParseArguments(args, options);

                IAmazonCloudFormation client = new AmazonCloudFormationClient();

                CreateStackRequest request = new CreateStackRequest();
                request.StackName = options.StackName;
                client.CreateStack(request);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Environment.Exit((int)ExitCode.Error);
            }

            Console.ReadKey();
            Environment.Exit((int)ExitCode.Success);

        }
    }
}
